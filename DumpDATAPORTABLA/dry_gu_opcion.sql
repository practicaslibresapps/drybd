-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dry
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `gu_opcion`
--

LOCK TABLES `gu_opcion` WRITE;
/*!40000 ALTER TABLE `gu_opcion` DISABLE KEYS */;
INSERT INTO `gu_opcion` VALUES (1,'USUARIOS','catalogo/usuarios',1),(2,'CLIENTES','catalogo/clientes',1),(3,'PRODUCTOS','catalogo/allproductos',1),(5,'FACTURA','facturacion/factura',2),(6,'CAJA','caja/caja',6),(7,'INSUMOS','catalogo/insumos',1),(8,'PROVEEDORES','catalogo/proveedores',1),(9,'SERVICIOS','catalogo/servicios',1),(11,'ORDENES DE SASTRERIA','ordenes/ordenes_sastreria',8),(12,'ORDENES DE LAVADO','ordenes/ordenes_lavado',8),(13,'ORDENES DE SECADO','ordenes/ordenes_secado',8),(14,'ORDENES DE PLANCHADO','ordenes/ordenes_planchado',8),(15,'ORDENES A ENTREGAR','ordenes/ordenes_entregado',8),(16,'MOVIMIENTOS','ingresos_egresos/',9),(17,'SALDO CAJA','reportes/reporte_saldo_caja',10),(18,'MOVIMIENTOS','reportes/reporte_movimientos',10),(19,'VENTAS CLIENTES','reportes/reporte_ventas_cliente',10),(20,'ORDENES DE TRABAJO','reportes/reporte_ordenes_trabajo',10),(21,'KARDEX','reportes/reporte_kardex',10);
/*!40000 ALTER TABLE `gu_opcion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-11 12:22:42
