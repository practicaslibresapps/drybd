-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dry
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `maestro_insumo`
--

LOCK TABLES `maestro_insumo` WRITE;
/*!40000 ALTER TABLE `maestro_insumo` DISABLE KEYS */;
INSERT INTO `maestro_insumo` VALUES (1,'Amitriptyline Hydrochloride','unidad'),(2,'Chlordiazepoxide Hydrochloride','unidad'),(3,'Petrolatum','unidad'),(4,'Simethicone','unidad'),(5,'Aluminum zirconium tetrachlorohydrex gly','unidad'),(6,'Levothyroxine Sodium','unidad'),(7,'tolazamide','unidad'),(8,'Ibuprofen','unidad'),(9,'Antiseptic handwash','unidad'),(10,'Hydralazine Hydrochloride','unidad'),(11,'Olanzapine','unidad'),(12,'Urea','unidad'),(13,'CALCIUM FLUORIDE','unidad'),(14,'hypromellose','unidad'),(15,'Menthol and Methyl Salicylate','unidad'),(16,'metoprolol succinate and hydrochlorothiazide','unidad'),(17,'INDOMETHACIN','unidad'),(18,'Titanium Dioxide and Zinc Oxide','unidad'),(19,'Triclosan','unidad'),(20,'Lamotrigine','unidad'),(21,'Dandelion','unidad'),(22,'DEXTROMETHORPHAN HYDROBROMIDE and DOXYLAMINE SUCCINATE','unidad'),(23,'White Mulberry','unidad'),(24,'Naratriptan','unidad'),(25,'Oxygen','unidad'),(26,'OCTINOXATE, OCTISALATE','unidad'),(27,'menthol','unidad'),(28,'Ceftriaxone Sodium','unidad'),(29,'Diclofenac Sodium','unidad'),(30,'butalbital, acetaminophen and caffeine','unidad'),(31,'ARIPIPRAZOLE','unidad'),(32,'ALCOHOL','unidad'),(33,'Ammonium Lactate','unidad'),(34,'CHLORDIAZEPOXIDE HYDROCHLORIDE and CLIDINIUM BROMIDE','unidad'),(35,'Ibuprofen','unidad'),(36,'Zolmitriptan','unidad'),(37,'Olanzapine','unidad'),(38,'Naproxen sodium','unidad'),(39,'carboxymethylcellulose sodium','unidad'),(40,'SERTRALINE HYDROCHLORIDE','unidad'),(41,'bacitracin, neomycin, polymyxin B','unidad'),(42,'Lansoprazole','unidad'),(43,'diltiazem','unidad'),(44,'Estradiol','unidad'),(45,'Carbidopa and levodopa','unidad'),(46,'Agnus castus e sem. 4','unidad'),(47,'TITANIUM DIOXIDE','unidad'),(48,'Phenol','unidad'),(49,'ALCOHOL','unidad'),(50,'benztropine mesylate','unidad'),(51,'QUETIAPINE FUMARATE','unidad'),(52,'Avobenzone, Octisalate, Octocrylene and Oxybenzone','unidad'),(53,'Titanium Dioxide and Octinoxate','unidad'),(54,'Ethyl Macadamiate','unidad'),(55,'OCTINOXATE, TITANIUM DIOXIDE, and ZINC OXIDE','unidad'),(56,'Benzalkonium chloride','unidad'),(57,'nicotine','unidad'),(58,'OCTINOXATE, OCTOCRYLENE, AVOBENZONE','unidad'),(59,'Nortriptyline Hydrochloride','unidad'),(60,'Avobenzone, Octinoxate, Octisalate, Oxybenzone','unidad'),(61,'Piroxicam','unidad'),(62,'Brompheniramine maleate, Dextromethorphan HBr, Phenylephrine HCl','unidad'),(63,'Chlorzoxazone','unidad'),(64,'MENTHOL','unidad'),(65,'Quetiapine fumarate','unidad'),(66,'Rosuvastatin calcium','unidad'),(67,'HOMOSALATE, OCTISALATE, OXYBENZONE, AVOBENZONE, OCTOCRYLENE','unidad'),(68,'Aconitum napellus, Hypericum perforatum, Lycopodium clavatum, Phosphorus, Rhus toxicodendron, Secale cornutum','unidad'),(69,'Cyclobenzaprine Hydrochloride','unidad'),(70,'Phenylephrine Hydrochloride','unidad'),(71,'Metronidazole','unidad'),(72,'Dextromethorphan HBr, Guaifenesin','unidad'),(73,'Cholecalciferol, Ascorbic Acid, Folic Acid, Thiamin, Riboflavin, Niacin, Pantothenic Acid, Pyridoxine HCl, Biotin, Cyanocobalamin','unidad'),(74,'Dill','unidad'),(75,'Lycopodium clavatum, Pulsatilla,','unidad'),(76,'PredniSONE','unidad'),(77,'Midazolam','unidad'),(78,'Ethyl Alcohol','unidad'),(79,'Divalproex Sodium','unidad'),(80,'Butalbital, Acetaminophen, Caffeine, and Codeine Phosphate','unidad'),(81,'Pelargonium sidoides','unidad'),(82,'Octinoxate and Titanium dioxide','unidad'),(83,'warfarin sodium','unidad'),(84,'Metoclopramide Hydrochloride','unidad'),(85,'AVOBENZONE, OCTINOXATE, OCTISALATE, OXYBENZONE','unidad'),(86,'Triclosan','unidad'),(87,'Sotalol Hydrochloride','unidad'),(88,'Fentanyl','unidad'),(89,'von Willebrand Factor/Coagulation Factor VIII Complex (Human)','unidad'),(90,'Angelica Archangelica, Caulophyllum Thalictroides, Disoscorea Villosa, Oophorinum (Suis) Thyroidinum (Suis), Uterus (Suis), Zincum Gluconicum','unidad'),(91,'White Petrolatum','unidad'),(92,'Quetiapine fumarate','unidad'),(93,'DOXOrubicin Hydrochloride','unidad'),(94,'White Petrolatum','unidad'),(95,'rifampin, isoniazid and pyrazinamide','unidad'),(96,'Bismuth Subsalicylate','unidad'),(97,'Ranitidine','unidad'),(98,'HYDROQUINONE','unidad'),(99,'mesna','unidad'),(100,'California Live Oak','unidad');
/*!40000 ALTER TABLE `maestro_insumo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-11 12:22:43
