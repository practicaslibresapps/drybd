-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dry
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `maestro_usuario`
--

LOCK TABLES `maestro_usuario` WRITE;
/*!40000 ALTER TABLE `maestro_usuario` DISABLE KEYS */;
INSERT INTO `maestro_usuario` VALUES (1,'admin','admin',1,'2015-06-25','Administrador',1,0),(2,'planchado','123',1,'2015-09-03','planchado',1,4),(3,'ENTREGAR','123',1,'2015-09-03','ENTREGAR',1,0),(4,'Sastreria','123',1,'2015-09-03','Sastreria',1,1),(5,'Lavado','123',1,'2015-09-03','Lavado',1,2),(6,'Secado','123',1,'2015-09-03','Secado',1,3),(7,'Por Entregar','123',1,'2015-09-03','Por Entregar',1,5),(8,'planchado2','123',1,'2015-09-03','planchado2',2,4),(9,'ENTREGAR2','123',1,'2015-09-03','ENTREGAR2',2,0),(10,'Sastreria2','123',1,'2015-09-03','Sastreria2',2,1),(11,'Lavado2','123',1,'2015-09-03','Lavado2',2,2),(12,'Secado2','123',1,'2015-09-03','Secado2',2,3),(13,'Por Entregar2','123',1,'2015-09-03','Por Entregar2',2,5),(14,'planchado3','123',1,'2015-09-03','Por Entregar3',3,4),(15,'ENTREGAR3','123',1,'2015-09-03','ENTREGAR3',3,0),(16,'Sastreria3','123',1,'2015-09-03','Sastreria3',3,1),(17,'Lavado3','123',1,'2015-09-03','Lavado3',3,2),(18,'Secado3','123',1,'2015-09-03','Secado3',3,3),(19,'Por Entregar3','123',1,'2015-09-03','Por Entregar3',3,5);
/*!40000 ALTER TABLE `maestro_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-11 12:22:44
