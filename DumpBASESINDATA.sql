-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dry
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `25062019_settings`
--

DROP TABLE IF EXISTS `25062019_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `25062019_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `valor` text NOT NULL,
  `simbolo` varchar(5) NOT NULL,
  `comentario` varchar(100) NOT NULL DEFAULT '',
  `activado` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 la configuracion se aplica/activa - 0 ña conf no se aplica',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `caja_movimiento`
--

DROP TABLE IF EXISTS `caja_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caja_movimiento` (
  `idcajamovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idfactura` int(11) NOT NULL DEFAULT '0',
  `tipomovimiento` int(11) NOT NULL DEFAULT '0',
  `billete_20` int(11) NOT NULL DEFAULT '0',
  `billete_10` int(11) NOT NULL DEFAULT '0',
  `billete_5` int(11) NOT NULL DEFAULT '0',
  `billete_1` int(11) NOT NULL DEFAULT '0',
  `moneda_1` int(11) NOT NULL DEFAULT '0',
  `moneda_25` int(11) NOT NULL DEFAULT '0',
  `moneda_10` int(11) NOT NULL DEFAULT '0',
  `moneda_5` int(11) NOT NULL DEFAULT '0',
  `moneda_01` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `observaciones` varchar(300) NOT NULL DEFAULT '',
  `totalfactura` double(12,2) NOT NULL DEFAULT '0.00',
  `fecha_proceso` datetime NOT NULL,
  PRIMARY KEY (`idcajamovimiento`),
  KEY `fk_caja_movimiento_factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_caja_movimiento_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cierre_movimiento`
--

DROP TABLE IF EXISTS `cierre_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cierre_movimiento` (
  `idtienda` int(2) unsigned zerofill NOT NULL,
  `tipomovimiento` char(20) NOT NULL,
  `cantidad` double(12,2) NOT NULL,
  `fecha` date NOT NULL,
  `numerodocumento` int(11) NOT NULL,
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(4) NOT NULL,
  `anulado` char(3) NOT NULL,
  `correlativo` int(11) NOT NULL AUTO_INCREMENT,
  `idinsumo` int(8) unsigned zerofill NOT NULL,
  `aniomes` int(11) DEFAULT '0',
  PRIMARY KEY (`correlativo`,`idtienda`,`numerodocumento`,`idinsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `factura_detalle`
--

DROP TABLE IF EXISTS `factura_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_detalle` (
  `idfactura_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `idfactura` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura_detalle`),
  KEY `fk_factura_detalle_factura_encabezado` (`idfactura`),
  KEY `fk_factura_detalle_maestro_servicio` (`idservicio`),
  CONSTRAINT `fk_factura_detalle_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_factura_detalle_maestro_servicio` FOREIGN KEY (`idservicio`) REFERENCES `maestro_servicio` (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `factura_encabezado`
--

DROP TABLE IF EXISTS `factura_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_encabezado` (
  `idfactura` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL DEFAULT '0',
  `fecha` datetime DEFAULT NULL,
  `idtipopago` int(11) NOT NULL DEFAULT '0',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `anulado` char(1) NOT NULL DEFAULT 'n',
  `idtienda` int(11) NOT NULL,
  `procesado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura`),
  KEY `fk_factura_encabezado_maestro_cliente` (`idcliente`),
  KEY `fk_factura_encabezado_tipo_pago_idx` (`idtipopago`),
  CONSTRAINT `fk_factura_encabezado_maestro_cliente` FOREIGN KEY (`idcliente`) REFERENCES `maestro_cliente` (`idcliente`),
  CONSTRAINT `fk_factura_encabezado_tipo_pago` FOREIGN KEY (`idtipopago`) REFERENCES `maestro_tipo_pago` (`idtipopago`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `factura_temp`
--

DROP TABLE IF EXISTS `factura_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_temp` (
  `idfactura` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `idtienda` int(11) DEFAULT NULL,
  `descripcion_servicio` varchar(300) DEFAULT NULL,
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gu_menu`
--

DROP TABLE IF EXISTS `gu_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `icono` varchar(350) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `movil` int(11) DEFAULT '0',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gu_opcion`
--

DROP TABLE IF EXISTS `gu_opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_opcion` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `opcion` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion`),
  KEY `fk_guopcion_menu_idx` (`id_menu`),
  CONSTRAINT `fk_guopcion_menu` FOREIGN KEY (`id_menu`) REFERENCES `gu_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gu_rol`
--

DROP TABLE IF EXISTS `gu_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_rol` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gu_rol_menu`
--

DROP TABLE IF EXISTS `gu_rol_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_rol_menu` (
  `id_rol` int(11) NOT NULL,
  `id_opcion` int(11) NOT NULL,
  KEY `fk_gurolmenu_guopcion_idx` (`id_opcion`),
  KEY `fk_gurolmenu_ro` (`id_rol`),
  CONSTRAINT `fk_gurolmenu_guopcion` FOREIGN KEY (`id_opcion`) REFERENCES `gu_opcion` (`id_opcion`),
  CONSTRAINT `fk_gurolmenu_ro` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_area`
--

DROP TABLE IF EXISTS `maestro_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_area` (
  `id_area` int(11) NOT NULL,
  `area` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_cliente`
--

DROP TABLE IF EXISTS `maestro_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `direccion` varchar(100) NOT NULL DEFAULT '',
  `idmunicipio` int(11) NOT NULL DEFAULT '0',
  `dui` char(10) NOT NULL DEFAULT '',
  `nit` char(17) NOT NULL DEFAULT '',
  `registro` char(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`idcliente`),
  KEY `fk_maestrocliente_maestromunicipio` (`idmunicipio`),
  CONSTRAINT `fk_maestrocliente_maestromunicipio` FOREIGN KEY (`idmunicipio`) REFERENCES `maestro_municipio` (`idmunicipio`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_insumo`
--

DROP TABLE IF EXISTS `maestro_insumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_insumo` (
  `idinsumo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(300) DEFAULT '',
  `unidad` char(30) DEFAULT NULL,
  PRIMARY KEY (`idinsumo`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_municipio`
--

DROP TABLE IF EXISTS `maestro_municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_municipio` (
  `idmunicipio` int(11) NOT NULL AUTO_INCREMENT,
  `departamento` varchar(60) NOT NULL DEFAULT '',
  `nombre` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`idmunicipio`),
  KEY `fk_maestromunicipio_maestrodepartamento` (`departamento`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_proveedor`
--

DROP TABLE IF EXISTS `maestro_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_proveedor` (
  `idproveedor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL DEFAULT '',
  `direccion` varchar(300) NOT NULL DEFAULT '',
  `telefono` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`idproveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_servicio`
--

DROP TABLE IF EXISTS `maestro_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_servicio` (
  `idservicio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(300) DEFAULT '',
  `precio` double(12,2) DEFAULT NULL,
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_tienda`
--

DROP TABLE IF EXISTS `maestro_tienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tienda` (
  `idtienda` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `ubicacion` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`idtienda`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_tipo_movimiento`
--

DROP TABLE IF EXISTS `maestro_tipo_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tipo_movimiento` (
  `idtipomovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL DEFAULT '',
  `operacion` char(1) DEFAULT '',
  PRIMARY KEY (`idtipomovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_tipo_pago`
--

DROP TABLE IF EXISTS `maestro_tipo_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tipo_pago` (
  `idtipopago` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) NOT NULL,
  PRIMARY KEY (`idtipopago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maestro_usuario`
--

DROP TABLE IF EXISTS `maestro_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_rol` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `nombre_completo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `idtienda` int(11) DEFAULT NULL,
  `id_area` int(11) DEFAULT '0',
  PRIMARY KEY (`id_usuario`),
  KEY `fk_maestrousuario_gurol` (`id_rol`),
  KEY `fk_maestrousuario_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_maestrousuario_gurol` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`),
  CONSTRAINT `fk_maestrousuario_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimiento_detalle`
--

DROP TABLE IF EXISTS `movimiento_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_detalle` (
  `idmovimiento_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `idmovimiento` int(11) NOT NULL DEFAULT '0',
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `cantidad` double(12,2) NOT NULL DEFAULT '0.00',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `iva` double(12,2) NOT NULL DEFAULT '0.00',
  `total` double(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idmovimiento_detalle`),
  KEY `fk_movimientodetalle_movimientoencabezado` (`idmovimiento`),
  KEY `fk_movimientodetalle_maestroinsumo` (`idinsumo`),
  CONSTRAINT `fk_movimientodetalle_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_movimientodetalle_movimientoencabezado` FOREIGN KEY (`idmovimiento`) REFERENCES `movimiento_encabezado` (`idmovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimiento_encabezado`
--

DROP TABLE IF EXISTS `movimiento_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_encabezado` (
  `idmovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `idtipomovimiento` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `idproveedor` int(11) NOT NULL DEFAULT '0',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_total` double NOT NULL DEFAULT '0',
  `documento` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmovimiento`),
  KEY `fk_movimientoencabezado_maestroproveedor` (`idproveedor`),
  KEY `fk_movimientoencabezado_maestrotipomovimiento` (`idtipomovimiento`),
  CONSTRAINT `fk_movimientoencabezado_maestroproveedor` FOREIGN KEY (`idproveedor`) REFERENCES `maestro_proveedor` (`idproveedor`),
  CONSTRAINT `fk_movimientoencabezado_maestrotipomovimiento` FOREIGN KEY (`idtipomovimiento`) REFERENCES `maestro_tipo_movimiento` (`idtipomovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimiento_temp`
--

DROP TABLE IF EXISTS `movimiento_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_temp` (
  `idmovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idproveedor` int(11) NOT NULL,
  `idinsumo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `total` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `idtienda` int(11) NOT NULL,
  `descripcion_insumo` varchar(300) NOT NULL,
  `idtipomovimiento` int(11) NOT NULL,
  `documento` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmovimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ot_encabezado`
--

DROP TABLE IF EXISTS `ot_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ot_encabezado` (
  `idorden` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `idfactura` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  `usuario_sastreria` int(11) NOT NULL DEFAULT '0',
  `usuario_lavado` int(11) NOT NULL DEFAULT '0',
  `usuario_secado` int(11) NOT NULL DEFAULT '0',
  `usuario_planchado` int(11) NOT NULL DEFAULT '0',
  `usuario_entregado` int(11) DEFAULT '0',
  `fecha_sastreria` datetime DEFAULT NULL,
  `fecha_lavado` datetime DEFAULT NULL,
  `fecha_secado` datetime DEFAULT NULL,
  `fecha_planchado` datetime DEFAULT NULL,
  `fecha_entregado` datetime DEFAULT NULL,
  `estado_sastreria` int(11) NOT NULL DEFAULT '0',
  `estado_lavado` int(11) NOT NULL DEFAULT '0',
  `estado_secado` int(11) NOT NULL DEFAULT '0',
  `estado_planchado` int(11) NOT NULL DEFAULT '0',
  `estado_entregado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idorden`),
  KEY `fk_factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saldos_cierre`
--

DROP TABLE IF EXISTS `saldos_cierre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saldos_cierre` (
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  `anio_mes` int(11) NOT NULL DEFAULT '0',
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  KEY `fk_saldoscierre_maestroinsumo` (`idinsumo`),
  KEY `fk_saldoscierre_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_saldoscierre_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_saldoscierre_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `saldos_insumo`
--

DROP TABLE IF EXISTS `saldos_insumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saldos_insumo` (
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  KEY `fk_saldosinsumo_maestroinsumo` (`idinsumo`),
  KEY `fk_saldostienda_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_saldosinsumo_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_saldostienda_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-11 12:23:26
