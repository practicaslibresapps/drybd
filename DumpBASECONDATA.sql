-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: dry
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `25062019_settings`
--

DROP TABLE IF EXISTS `25062019_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `25062019_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `valor` text NOT NULL,
  `simbolo` varchar(5) NOT NULL,
  `comentario` varchar(100) NOT NULL DEFAULT '',
  `activado` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 la configuracion se aplica/activa - 0 ña conf no se aplica',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `25062019_settings`
--

LOCK TABLES `25062019_settings` WRITE;
/*!40000 ALTER TABLE `25062019_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `25062019_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caja_movimiento`
--

DROP TABLE IF EXISTS `caja_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `caja_movimiento` (
  `idcajamovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idfactura` int(11) NOT NULL DEFAULT '0',
  `tipomovimiento` int(11) NOT NULL DEFAULT '0',
  `billete_20` int(11) NOT NULL DEFAULT '0',
  `billete_10` int(11) NOT NULL DEFAULT '0',
  `billete_5` int(11) NOT NULL DEFAULT '0',
  `billete_1` int(11) NOT NULL DEFAULT '0',
  `moneda_1` int(11) NOT NULL DEFAULT '0',
  `moneda_25` int(11) NOT NULL DEFAULT '0',
  `moneda_10` int(11) NOT NULL DEFAULT '0',
  `moneda_5` int(11) NOT NULL DEFAULT '0',
  `moneda_01` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `observaciones` varchar(300) NOT NULL DEFAULT '',
  `totalfactura` double(12,2) NOT NULL DEFAULT '0.00',
  `fecha_proceso` datetime NOT NULL,
  PRIMARY KEY (`idcajamovimiento`),
  KEY `fk_caja_movimiento_factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_caja_movimiento_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja_movimiento`
--

LOCK TABLES `caja_movimiento` WRITE;
/*!40000 ALTER TABLE `caja_movimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `caja_movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cierre_movimiento`
--

DROP TABLE IF EXISTS `cierre_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cierre_movimiento` (
  `idtienda` int(2) unsigned zerofill NOT NULL,
  `tipomovimiento` char(20) NOT NULL,
  `cantidad` double(12,2) NOT NULL,
  `fecha` date NOT NULL,
  `numerodocumento` int(11) NOT NULL,
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(4) NOT NULL,
  `anulado` char(3) NOT NULL,
  `correlativo` int(11) NOT NULL AUTO_INCREMENT,
  `idinsumo` int(8) unsigned zerofill NOT NULL,
  `aniomes` int(11) DEFAULT '0',
  PRIMARY KEY (`correlativo`,`idtienda`,`numerodocumento`,`idinsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cierre_movimiento`
--

LOCK TABLES `cierre_movimiento` WRITE;
/*!40000 ALTER TABLE `cierre_movimiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `cierre_movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_detalle`
--

DROP TABLE IF EXISTS `factura_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_detalle` (
  `idfactura_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `idfactura` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura_detalle`),
  KEY `fk_factura_detalle_factura_encabezado` (`idfactura`),
  KEY `fk_factura_detalle_maestro_servicio` (`idservicio`),
  CONSTRAINT `fk_factura_detalle_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_factura_detalle_maestro_servicio` FOREIGN KEY (`idservicio`) REFERENCES `maestro_servicio` (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_detalle`
--

LOCK TABLES `factura_detalle` WRITE;
/*!40000 ALTER TABLE `factura_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_encabezado`
--

DROP TABLE IF EXISTS `factura_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_encabezado` (
  `idfactura` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL DEFAULT '0',
  `fecha` datetime DEFAULT NULL,
  `idtipopago` int(11) NOT NULL DEFAULT '0',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `anulado` char(1) NOT NULL DEFAULT 'n',
  `idtienda` int(11) NOT NULL,
  `procesado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura`),
  KEY `fk_factura_encabezado_maestro_cliente` (`idcliente`),
  KEY `fk_factura_encabezado_tipo_pago_idx` (`idtipopago`),
  CONSTRAINT `fk_factura_encabezado_maestro_cliente` FOREIGN KEY (`idcliente`) REFERENCES `maestro_cliente` (`idcliente`),
  CONSTRAINT `fk_factura_encabezado_tipo_pago` FOREIGN KEY (`idtipopago`) REFERENCES `maestro_tipo_pago` (`idtipopago`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_encabezado`
--

LOCK TABLES `factura_encabezado` WRITE;
/*!40000 ALTER TABLE `factura_encabezado` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura_encabezado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_temp`
--

DROP TABLE IF EXISTS `factura_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura_temp` (
  `idfactura` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_bruto` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_descuento` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `idtienda` int(11) DEFAULT NULL,
  `descripcion_servicio` varchar(300) DEFAULT NULL,
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idfactura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_temp`
--

LOCK TABLES `factura_temp` WRITE;
/*!40000 ALTER TABLE `factura_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_menu`
--

DROP TABLE IF EXISTS `gu_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `icono` varchar(350) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `movil` int(11) DEFAULT '0',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_menu`
--

LOCK TABLES `gu_menu` WRITE;
/*!40000 ALTER TABLE `gu_menu` DISABLE KEYS */;
INSERT INTO `gu_menu` VALUES (1,'MAESTROS',NULL,0),(2,'FACTURACION',NULL,0),(3,'CONSULTAS',NULL,0),(4,'ORDENES TRABAJO',NULL,0),(5,'INVENTARIO',NULL,0),(6,'CAJA',NULL,0),(8,'ORDENES','',0),(9,'INGRESOS Y EGRESOS',NULL,0),(10,'REPORTES','fa fa-document',1);
/*!40000 ALTER TABLE `gu_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_opcion`
--

DROP TABLE IF EXISTS `gu_opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_opcion` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `opcion` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_opcion`),
  KEY `fk_guopcion_menu_idx` (`id_menu`),
  CONSTRAINT `fk_guopcion_menu` FOREIGN KEY (`id_menu`) REFERENCES `gu_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_opcion`
--

LOCK TABLES `gu_opcion` WRITE;
/*!40000 ALTER TABLE `gu_opcion` DISABLE KEYS */;
INSERT INTO `gu_opcion` VALUES (1,'USUARIOS','catalogo/usuarios',1),(2,'CLIENTES','catalogo/clientes',1),(3,'PRODUCTOS','catalogo/allproductos',1),(5,'FACTURA','facturacion/factura',2),(6,'CAJA','caja/caja',6),(7,'INSUMOS','catalogo/insumos',1),(8,'PROVEEDORES','catalogo/proveedores',1),(9,'SERVICIOS','catalogo/servicios',1),(11,'ORDENES DE SASTRERIA','ordenes/ordenes_sastreria',8),(12,'ORDENES DE LAVADO','ordenes/ordenes_lavado',8),(13,'ORDENES DE SECADO','ordenes/ordenes_secado',8),(14,'ORDENES DE PLANCHADO','ordenes/ordenes_planchado',8),(15,'ORDENES A ENTREGAR','ordenes/ordenes_entregado',8),(16,'MOVIMIENTOS','ingresos_egresos/',9),(17,'SALDO CAJA','reportes/reporte_saldo_caja',10),(18,'MOVIMIENTOS','reportes/reporte_movimientos',10),(19,'VENTAS CLIENTES','reportes/reporte_ventas_cliente',10),(20,'ORDENES DE TRABAJO','reportes/reporte_ordenes_trabajo',10),(21,'KARDEX','reportes/reporte_kardex',10);
/*!40000 ALTER TABLE `gu_opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_rol`
--

DROP TABLE IF EXISTS `gu_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_rol` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_rol`
--

LOCK TABLES `gu_rol` WRITE;
/*!40000 ALTER TABLE `gu_rol` DISABLE KEYS */;
INSERT INTO `gu_rol` VALUES (1,'admin'),(7,'sastreria'),(9,'oryeba');
/*!40000 ALTER TABLE `gu_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_rol_menu`
--

DROP TABLE IF EXISTS `gu_rol_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gu_rol_menu` (
  `id_rol` int(11) NOT NULL,
  `id_opcion` int(11) NOT NULL,
  KEY `fk_gurolmenu_guopcion_idx` (`id_opcion`),
  KEY `fk_gurolmenu_ro` (`id_rol`),
  CONSTRAINT `fk_gurolmenu_guopcion` FOREIGN KEY (`id_opcion`) REFERENCES `gu_opcion` (`id_opcion`),
  CONSTRAINT `fk_gurolmenu_ro` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_rol_menu`
--

LOCK TABLES `gu_rol_menu` WRITE;
/*!40000 ALTER TABLE `gu_rol_menu` DISABLE KEYS */;
INSERT INTO `gu_rol_menu` VALUES (1,1),(1,2),(1,5),(1,6),(1,7),(1,8),(1,9),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(7,11),(9,15),(1,17),(1,18),(1,19),(1,20),(1,21);
/*!40000 ALTER TABLE `gu_rol_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_area`
--

DROP TABLE IF EXISTS `maestro_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_area` (
  `id_area` int(11) NOT NULL,
  `area` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_area`
--

LOCK TABLES `maestro_area` WRITE;
/*!40000 ALTER TABLE `maestro_area` DISABLE KEYS */;
INSERT INTO `maestro_area` VALUES (0,'ENTREGA'),(1,'SASTRERIA'),(2,'LAVADO'),(3,'SECADO'),(4,'PLANCHADO'),(5,'POR ENTREGAR');
/*!40000 ALTER TABLE `maestro_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_cliente`
--

DROP TABLE IF EXISTS `maestro_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `direccion` varchar(100) NOT NULL DEFAULT '',
  `idmunicipio` int(11) NOT NULL DEFAULT '0',
  `dui` char(10) NOT NULL DEFAULT '',
  `nit` char(17) NOT NULL DEFAULT '',
  `registro` char(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`idcliente`),
  KEY `fk_maestrocliente_maestromunicipio` (`idmunicipio`),
  CONSTRAINT `fk_maestrocliente_maestromunicipio` FOREIGN KEY (`idmunicipio`) REFERENCES `maestro_municipio` (`idmunicipio`)
) ENGINE=InnoDB AUTO_INCREMENT=501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_cliente`
--

LOCK TABLES `maestro_cliente` WRITE;
/*!40000 ALTER TABLE `maestro_cliente` DISABLE KEYS */;
INSERT INTO `maestro_cliente` VALUES (1,'Janeva Harrigan','7 Gateway Junction',236,'325154485','673933518','1'),(2,'Morse Baynham','2 Summerview Alley',221,'740975448','289255126','2'),(3,'Bernadette Ingerson','4377 Petterle Drive',82,'432006510','199152977','3'),(4,'Danni Selwin','0598 Spohn Circle',170,'304551090','517178671','4'),(5,'Michal Vedenyakin','7 Barby Parkway',2,'562591037','573863163','5'),(6,'Oliver Keech','9 Burrows Court',121,'137583236','461484140','6'),(7,'Natty Cottesford','5851 Memorial Trail',5,'794529701','670630084','7'),(8,'Carmella Crosdill','0 Walton Court',237,'765057197','359717434','8'),(9,'Rozamond De la Harpe','91680 Hollow Ridge Trail',118,'605864522','272156951','9'),(10,'Willard Cornelius','5 Kings Pass',47,'584950688','524160003','10'),(11,'Lindie Chapelhow','0 Warrior Pass',185,'276688548','138708536','11'),(12,'Audy Benton','9 Bartelt Circle',230,'651765099','155450122','12'),(13,'Cindi Zylberdik','0836 Comanche Circle',149,'570765114','324797690','13'),(14,'Garland Alcalde','7 Oak Lane',156,'259866223','367627778','14'),(15,'Shana Halton','69 Toban Hill',42,'109687239','599320416','15'),(16,'Danya Scanlon','935 Packers Park',164,'513009992','235743835','16'),(17,'Blanche Schimke','7255 Gale Trail',225,'208784519','486670594','17'),(18,'Saraann Iorns','5584 Coolidge Crossing',56,'578168301','120509577','18'),(19,'Harriet Bucky','5005 Meadow Vale Terrace',78,'473649867','457501334','19'),(20,'Carlota Ranscombe','1 Talmadge Drive',128,'331031061','251397714','20'),(21,'Daniel Rabley','039 Lake View Hill',41,'795539806','179791051','21'),(22,'Gradeigh Dod','9 Lotheville Way',186,'557438058','522183470','22'),(23,'Benson Brinkman','403 Sunbrook Hill',186,'499547691','278556245','23'),(24,'Zebadiah Swapp','3 Calypso Park',119,'485647171','644372277','24'),(25,'Susy Wagstaff','4 Saint Paul Park',259,'517408507','543082157','25'),(26,'Trstram Filipyev','94 Blaine Lane',129,'707891448','336819023','26'),(27,'Kary Adamson','31 Washington Trail',199,'361592908','637020198','27'),(28,'Lilia Pethybridge','6000 Sage Court',141,'341328078','142986587','28'),(29,'Petey Ivons','923 Forster Court',119,'395954650','606255579','29'),(30,'Aura Johannes','5 Sugar Trail',203,'354496954','690101775','30'),(31,'Edvard Monckman','3 Spenser Avenue',31,'616696116','530829774','31'),(32,'Filippa Saltwell','52 Lillian Plaza',156,'778334677','590782275','32'),(33,'Gwen Swindle','5 Namekagon Terrace',41,'679071377','525250322','33'),(34,'Correy Goane','2 Gale Court',11,'629656694','522273108','34'),(35,'Denver Dyka','6 La Follette Way',234,'689552046','617041440','35'),(36,'Betteann Brussels','06244 Village Terrace',106,'147472144','202553814','36'),(37,'Jerrilee Scardifeild','5644 Talmadge Place',262,'730503556','397639735','37'),(38,'Neile Haistwell','3489 Di Loreto Point',219,'177386199','288869350','38'),(39,'Nicolais Showering','312 Sherman Hill',192,'726655918','177991939','39'),(40,'Blanche Folbig','2 Eliot Way',94,'176333849','707656147','40'),(41,'Sigismond Manoch','92457 Loeprich Court',186,'439973062','653118940','41'),(42,'Koressa Osan','7 Stephen Crossing',222,'457733697','331757505','42'),(43,'Hynda Gethins','560 Crownhardt Terrace',223,'510584101','441067642','43'),(44,'Theodosia Putterill','7472 Stephen Center',10,'196938126','226503772','44'),(45,'Tedman Tellett','7 Northland Hill',127,'348678814','761025628','45'),(46,'Si Mcmanaman','6 Armistice Court',209,'328480674','684486967','46'),(47,'Ashely Beiderbecke','146 Utah Plaza',43,'109326606','336247459','47'),(48,'Nesta Whild','596 Basil Avenue',102,'197592297','287648540','48'),(49,'Morgen Bernier','894 Sundown Drive',168,'741356819','553472254','49'),(50,'Betta Dorrian','10 Macpherson Junction',23,'612515512','284365025','50'),(51,'Pennie Joly','3 Boyd Drive',76,'252076950','186524772','51'),(52,'Albie Aslott','602 Bellgrove Parkway',50,'360180715','766492554','52'),(53,'Ethelbert McAleese','5 Stang Center',259,'516826963','283762078','53'),(54,'Carlen McElroy','9 Commercial Terrace',224,'427511271','719936324','54'),(55,'Mab Agneau','63 Lukken Parkway',231,'391931638','504185185','55'),(56,'Dale Gilluley','520 Everett Hill',4,'218293527','599128583','56'),(57,'Lutero Mary','173 Hooker Terrace',157,'593545728','701370723','57'),(58,'Mack Pearmine','46712 Harbort Hill',243,'139586519','462585053','58'),(59,'Stormie Bellino','57592 Bartillon Pass',116,'658409014','467326118','59'),(60,'Felicio Kerner','105 Orin Terrace',89,'287626960','539209582','60'),(61,'Anton Derrick','42618 Northwestern Way',82,'544497474','516806090','61'),(62,'Troy Lecordier','93796 Sutteridge Pass',235,'676176313','234986305','62'),(63,'Karine Espino','418 Evergreen Lane',184,'439839568','438946127','63'),(64,'Orbadiah Crossby','64 Sommers Court',125,'386954296','264688804','64'),(65,'Mattheus Wafer','21794 Monument Alley',195,'363633401','242672017','65'),(66,'Gertie Lomasna','0636 Vidon Junction',7,'785320901','510602856','66'),(67,'Bill Simoneschi','1237 Evergreen Lane',182,'669946274','440534254','67'),(68,'Hildagard Helks','095 Brentwood Terrace',183,'353625127','408644539','68'),(69,'Reginald Treneer','0 Westport Alley',228,'709497158','425330989','69'),(70,'Anne-corinne Tyce','4795 Sheridan Drive',102,'410719749','531795007','70'),(71,'Rici Lotherington','422 Lotheville Park',101,'388355638','351213477','71'),(72,'Sula Pennetti','917 Springview Alley',133,'474377030','307744458','72'),(73,'Malory Daniely','76 Toban Drive',173,'770480888','603351608','73'),(74,'Georgie Ellerby','317 Maywood Place',100,'183717769','710728559','74'),(75,'Sonny O\'Brogan','48697 Fieldstone Lane',101,'381988414','184045833','75'),(76,'Fred Reppaport','87 Crescent Oaks Way',109,'585823871','200827816','76'),(77,'Meriel Rahill','97462 High Crossing Crossing',171,'359620403','373107478','77'),(78,'Roman Tennison','4472 Thierer Avenue',195,'578504322','546274805','78'),(79,'Munroe McNair','595 Thierer Pass',197,'620997804','382943244','79'),(80,'Cully Forcade','072 Village Green Court',107,'479531889','546580880','80'),(81,'Fin McCart','9505 4th Circle',40,'689655792','184990368','81'),(82,'Selia MacCosty','32058 Fieldstone Road',40,'735237026','211505131','82'),(83,'Silas Janaway','64295 Green Ridge Avenue',175,'709877720','568177639','83'),(84,'Aprilette Haberfield','32723 Jay Plaza',45,'173578795','204919329','84'),(85,'Dasi Springthorpe','3721 Oxford Center',72,'733155786','675489916','85'),(86,'Winny Dunphie','4624 Everett Place',101,'388318386','763445542','86'),(87,'Kathe Soans','8 Monterey Pass',65,'265942383','711805886','87'),(88,'Jeanette Sayburn','839 Anthes Way',91,'279644376','260261062','88'),(89,'Byrle Halbert','63 Independence Circle',68,'656815630','405834130','89'),(90,'Guntar Barkway','1386 Fallview Road',170,'798695530','271736776','90'),(91,'Alex Beckley','36 Weeping Birch Hill',213,'140128072','116396386','91'),(92,'Tarah Pulley','0059 Stone Corner Alley',142,'781374803','532438277','92'),(93,'Dallis Kennelly','39935 Melby Trail',235,'739386373','574217166','93'),(94,'Arabela Issard','17 Dakota Street',148,'686221764','655962063','94'),(95,'Cyndie Mottershead','07 Warner Place',209,'474269501','569112909','95'),(96,'Lukas Ogley','219 Colorado Hill',114,'279561602','309253670','96'),(97,'Latia Parradine','358 Main Lane',146,'502357902','171246624','97'),(98,'Sanders Aizlewood','8189 Laurel Terrace',136,'587620007','690063417','98'),(99,'Franky Kilfeder','62 Walton Street',202,'125948446','573211736','99'),(100,'Dmitri Arnaez','0271 Algoma Point',240,'560628140','149369398','100'),(101,'Andrus Posselwhite','7 Merry Place',59,'422848893','346521841','101'),(102,'Pattin McCuaig','06244 Caliangt Drive',91,'635190880','394756268','102'),(103,'Antons Bakster','2685 Moland Plaza',79,'320981009','233610452','103'),(104,'Heywood Dimitrescu','49490 Lerdahl Road',18,'787828862','370665560','104'),(105,'Zerk Schimmang','36391 Columbus Plaza',229,'202509001','794627877','105'),(106,'Abby Dowe','97355 Mendota Lane',64,'703429098','657471986','106'),(107,'Krysta Stebbings','3785 Pankratz Circle',112,'450734362','781818746','107'),(108,'Amara Marlow','079 Vidon Hill',201,'379270457','467728993','108'),(109,'Victor Couzens','2207 Springs Drive',41,'388547446','144497230','109'),(110,'Paige Linstead','95291 Browning Avenue',247,'321207269','634779856','110'),(111,'Johnny Peschka','535 Mallard Alley',221,'627214852','241992771','111'),(112,'Nancy Masic','0 Lien Point',68,'585644952','469350840','112'),(113,'Daisey Croy','60 Fulton Hill',174,'499891671','577610999','113'),(114,'Muriel Dane','5725 Dwight Parkway',182,'170681789','608978750','114'),(115,'Perkin Tappington','5 Garrison Drive',103,'643941110','250704537','115'),(116,'Corella Silverlock','76445 Morningstar Center',190,'632909428','102216972','116'),(117,'Danell Wiggin','1 Spenser Junction',65,'516547496','390042502','117'),(118,'Nico Winstone','1 Raven Trail',233,'122089916','147946612','118'),(119,'Perrine Ivermee','87079 Ronald Regan Road',131,'432662694','129143217','119'),(120,'Wainwright Kencott','313 Cherokee Junction',232,'173573801','132953208','120'),(121,'Kirby Goncalo','75012 Spohn Plaza',249,'598131707','628041401','121'),(122,'Leshia Lippard','54 Dunning Parkway',15,'581526466','793204958','122'),(123,'Riccardo Sailer','687 Spenser Street',37,'719004182','389607381','123'),(124,'Linet Dussy','3993 Fairview Park',107,'208486547','749866719','124'),(125,'Valaree Gawne','34 Caliangt Plaza',54,'282019786','460087162','125'),(126,'Barnaby Oppy','76 West Center',220,'223531196','121254104','126'),(127,'Jarrid Heiden','5 Bashford Alley',98,'616156443','630899667','127'),(128,'Ebeneser Connors','2248 Blue Bill Park Way',41,'658522284','395462483','128'),(129,'Joey Rame','82222 Nobel Drive',184,'409636427','331938188','129'),(130,'Heidie Jakes','12 Lighthouse Bay Parkway',155,'102789184','405829078','130'),(131,'Byron Towe','60566 Rigney Way',175,'617898108','107260959','131'),(132,'Katina Rosenfrucht','294 Melby Point',60,'152764293','717292508','132'),(133,'Gerri Otridge','4 Wayridge Lane',67,'491287689','249768937','133'),(134,'Sabina Millward','3 Bayside Hill',37,'162914891','256772591','134'),(135,'Theresita Speaks','85451 Bayside Drive',80,'325279778','374110250','135'),(136,'Hardy Christoffersen','1 Basil Court',168,'116039721','322899462','136'),(137,'Blisse Duplock','32220 Pierstorff Hill',108,'449480016','712401128','137'),(138,'Frederich Ornelas','5 Cascade Drive',59,'237553736','245758184','138'),(139,'Tiffy Ebden','063 Eliot Point',228,'380354368','659186812','139'),(140,'Ethelbert Falco','66 Magdeline Drive',155,'504030453','663954172','140'),(141,'Greer Gallaccio','6530 Longview Drive',84,'636368087','523288028','141'),(142,'Ginger Trevna','84 Superior Trail',146,'235309319','416380402','142'),(143,'Laurel Trotman','7 Rusk Terrace',27,'596945304','101769853','143'),(144,'Giles Crummey','621 Columbus Court',113,'348481797','584107949','144'),(145,'Llywellyn Tomczak','466 Division Court',231,'251422062','547300636','145'),(146,'Jacquenette Lantaff','0618 Mandrake Place',170,'776062737','538952231','146'),(147,'Daveta Spooner','18 Fairview Alley',45,'390216939','385765194','147'),(148,'Kaiser Florez','067 Arizona Center',184,'193465037','721474396','148'),(149,'Yorker Ovens','77 Corry Avenue',240,'781128634','316525701','149'),(150,'Devi Forst','31 Blue Bill Park Center',125,'730304964','225306486','150'),(151,'Mariska Burgis','9 Mayfield Place',25,'692025628','208132720','151'),(152,'Fallon Skocroft','9 Northland Crossing',217,'762664544','346170648','152'),(153,'Kort Uc','05 Ruskin Alley',146,'644133321','553582760','153'),(154,'Fae Doak','73624 Carioca Junction',217,'633544061','757863715','154'),(155,'Sayre Piscopello','714 Mitchell Plaza',15,'690506005','436998693','155'),(156,'Caressa Ragbourn','569 Lien Junction',197,'148398379','729494413','156'),(157,'Salvidor Newburn','2 Hallows Parkway',65,'276202394','708812730','157'),(158,'Aloisia Morphew','429 Buell Point',233,'460050635','167713473','158'),(159,'Drucill Clunan','23 Toban Street',124,'436432999','493924807','159'),(160,'Giraud Mayhou','4 Arrowood Hill',7,'301684551','486175319','160'),(161,'Odelinda Elks','45982 Tomscot Crossing',250,'161916221','626660809','161'),(162,'Ange Boness','35316 Holy Cross Lane',261,'612690186','465469398','162'),(163,'Amy Coggon','0 Parkside Avenue',217,'560410191','423328646','163'),(164,'Carita Albers','9 2nd Alley',19,'570291173','609408821','164'),(165,'Mei Mordacai','34940 Kim Pass',172,'644851255','480186090','165'),(166,'Cherida Safont','0 Westridge Center',166,'723026722','109691248','166'),(167,'Corly Apps','08 Old Shore Parkway',234,'547256736','600655637','167'),(168,'Jerrylee Paskerful','3767 Starling Court',7,'100588367','387701348','168'),(169,'Clark Lowdham','4 Emmet Pass',192,'319575168','378808691','169'),(170,'Ganny Galia','85004 Hoepker Court',156,'488375941','213045780','170'),(171,'Kiah Denington','48 Swallow Way',182,'328389650','429769657','171'),(172,'Skelly Kensall','42689 Butterfield Way',202,'255330774','694486256','172'),(173,'Ginger Branchet','2 Sutteridge Trail',82,'776580377','185357818','173'),(174,'Bert Schorah','478 Derek Avenue',137,'579334878','427754931','174'),(175,'Hobey Gaythorpe','96 Hintze Parkway',160,'165844596','683444079','175'),(176,'Renado Thomton','7537 David Place',157,'674016613','512132932','176'),(177,'Stacee Littleover','3 Warbler Pass',104,'785354644','679120650','177'),(178,'Niels Sebright','80 Heath Trail',251,'236410053','142280023','178'),(179,'Clyve Rosensaft','9 Pawling Plaza',228,'210744225','384276027','179'),(180,'Lynnea Meharry','5 Division Crossing',25,'158417046','571094719','180'),(181,'Nil Jukes','51 Mayfield Court',216,'477235366','319964218','181'),(182,'Shoshana Orrobin','659 Village Lane',248,'750938560','391833458','182'),(183,'Christal Northen','50 Bunker Hill Way',200,'427176004','727392841','183'),(184,'Merry Archbell','15 Cordelia Road',112,'615917602','389400613','184'),(185,'Candis Chislett','09788 Armistice Place',74,'742804312','712165167','185'),(186,'Ellette Commucci','8 Grasskamp Lane',121,'246531470','673556249','186'),(187,'Rosaline Brady','795 Dayton Park',155,'674105818','475550598','187'),(188,'Liam Penny','7435 Bayside Street',110,'684920670','755094771','188'),(189,'Cassi Bassam','51170 Dovetail Circle',90,'739026570','649117491','189'),(190,'Wini Skynner','5 Morningstar Drive',6,'145119783','709810311','190'),(191,'Cliff Duffan','31 Menomonie Court',2,'339654715','504273504','191'),(192,'Willabella Putnam','5894 Briar Crest Center',7,'640154448','280900672','192'),(193,'Jolyn Sedcole','1 Nevada Lane',182,'544183700','567758546','193'),(194,'Bab Beininck','71 Longview Alley',34,'121859563','791454977','194'),(195,'Arlie Englefield','433 6th Terrace',202,'320350859','535590387','195'),(196,'Garwood Sproule','8517 Glacier Hill Crossing',11,'442818426','687261313','196'),(197,'Lavinie Ankers','174 Forster Drive',43,'223108410','667535020','197'),(198,'Fallon Menichelli','5709 Tennyson Trail',50,'314896985','273311372','198'),(199,'Obadias Lodford','406 Dawn Circle',115,'644387527','695960957','199'),(200,'Lazar Kurten','4346 Parkside Terrace',133,'594614186','231714672','200'),(201,'Dionisio Collison','00908 Carioca Avenue',92,'325795577','746071535','201'),(202,'Allyn Breheny','49549 Sherman Street',163,'420874695','394787410','202'),(203,'Suzi Catterick','7 Burning Wood Junction',123,'775576824','180786921','203'),(204,'Kaitlyn Tesyro','57698 Mockingbird Parkway',8,'155999854','433496961','204'),(205,'Ferne Edelmann','44036 Dorton Point',204,'429279396','219790585','205'),(206,'Alon Foyster','6 Surrey Terrace',149,'480626366','713024603','206'),(207,'Berta Fawcett','237 Clarendon Place',215,'641512470','721210254','207'),(208,'Melissa Brosio','11321 Wayridge Center',191,'652404024','372515363','208'),(209,'Jeffrey Walshaw','27823 Mitchell Avenue',193,'269312702','561990029','209'),(210,'Jonah Kertess','69 Green Ridge Trail',44,'418928442','453849064','210'),(211,'Saba Zimmermanns','13482 Farragut Place',20,'697260141','296197660','211'),(212,'Melody Grzegorzewski','822 Onsgard Road',90,'478980396','408393246','212'),(213,'Si Obray','877 Transport Drive',124,'591924822','646530759','213'),(214,'Tiphani Crichten','2094 Hollow Ridge Terrace',244,'441593061','250759007','214'),(215,'Mick Duesbury','95 Linden Lane',260,'774716556','186317472','215'),(216,'Mathias Wilbud','7 Kenwood Street',237,'671274432','473106842','216'),(217,'Katee Gailor','4 Fallview Junction',103,'513344995','467629110','217'),(218,'Reynard Dalyiel','05963 Milwaukee Center',205,'614658734','531222655','218'),(219,'Abelard Vakhonin','98133 Corben Junction',101,'637304255','306970065','219'),(220,'Salem Le Page','2520 Evergreen Point',248,'153999085','124545354','220'),(221,'Holmes Warnock','69517 Del Mar Pass',23,'250246426','250722902','221'),(222,'Anet Belcham','6167 Scoville Point',38,'450204274','580788206','222'),(223,'Arturo Lecointe','50244 Oak Valley Court',220,'180147119','325226700','223'),(224,'Neill Bloxsome','187 Express Pass',125,'454572872','357122260','224'),(225,'Lucas Cromblehome','24 Farragut Lane',255,'377381573','662639665','225'),(226,'Rockie Zanuciolii','8786 Grover Terrace',43,'375642540','797662883','226'),(227,'Hubie Bramont','93337 Ridge Oak Hill',68,'641703844','363391365','227'),(228,'Gaynor Cowles','389 Carioca Alley',222,'722483796','369414487','228'),(229,'Saul McShee','463 Jenna Junction',6,'734953250','583385860','229'),(230,'Conny Tarver','5820 Butternut Circle',87,'390846578','403221339','230'),(231,'Mirabelle Ector','1104 Gulseth Avenue',249,'203646125','497515246','231'),(232,'Delia Skotcher','79245 Dryden Plaza',229,'141892089','684646310','232'),(233,'Nico McGregor','9 Raven Street',262,'527734185','355112962','233'),(234,'Jaquith Madeley','121 Crowley Terrace',256,'349635431','249333890','234'),(235,'Brnaba Pykerman','282 Eastwood Trail',259,'603870477','274983020','235'),(236,'Kassie Desson','97628 Blaine Street',78,'631527280','401889248','236'),(237,'Tadeo Biford','5997 Cascade Point',170,'630028636','757496741','237'),(238,'Shanan Cantillon','1 Brown Road',121,'230606293','240207863','238'),(239,'Livvy Clappison','1 Glendale Park',86,'763026469','241248775','239'),(240,'Malva Hutcheson','879 Main Junction',9,'112732181','450050723','240'),(241,'Babbette Dudley','423 Village Green Trail',109,'377468213','591533824','241'),(242,'Cleveland Addey','0 Carberry Crossing',29,'352748871','523269975','242'),(243,'Parrnell Goodswen','8 Straubel Center',193,'629238247','662521663','243'),(244,'Ulrich Huckle','71802 Pennsylvania Street',192,'668756438','680689390','244'),(245,'Franky Papachristophorou','9 Upham Parkway',186,'532191050','524004707','245'),(246,'Antonella Haberjam','65755 Donald Plaza',151,'145603012','611400411','246'),(247,'Helsa Calder','479 Green Ridge Road',22,'635959755','205316162','247'),(248,'Torrence Spring','3865 Towne Junction',229,'510683807','446494470','248'),(249,'Jervis McGibbon','598 Amoth Junction',32,'688721440','505904974','249'),(250,'Alva Kimm','145 Muir Circle',201,'345201469','705121702','250'),(251,'Athene Napper','8033 Lillian Plaza',131,'333688413','436418958','251'),(252,'Lisabeth Pesticcio','36830 Orin Park',259,'796199181','228274335','252'),(253,'Olvan Ilden','1205 Paget Center',17,'595344994','496553839','253'),(254,'Mac Vanshin','71042 Oakridge Terrace',253,'773274199','643768281','254'),(255,'Kacie Butland','096 Lunder Lane',58,'556629232','461709562','255'),(256,'Anallise Valenti','08 Glendale Point',17,'500060604','596971689','256'),(257,'Rorke Gravy','405 Gerald Trail',253,'719386720','663266513','257'),(258,'Farly Vassel','6108 Pankratz Trail',7,'245714125','117539748','258'),(259,'Kelbee Dansey','5 Miller Avenue',159,'787181836','187988135','259'),(260,'Humberto Philipps','984 Northport Junction',22,'576970797','776979772','260'),(261,'Dickie Meads','376 Dovetail Lane',196,'170354385','525980262','261'),(262,'Winthrop Milsted','17901 Fallview Pass',145,'335611154','671288488','262'),(263,'Jania Whopples','2 Mayfield Parkway',197,'408483179','632436927','263'),(264,'Patrice Dicker','151 Steensland Crossing',21,'277107479','518793301','264'),(265,'Maribeth Klimkov','5 Harper Crossing',52,'217643603','570324529','265'),(266,'Hi Tracy','5 Manley Parkway',250,'167570218','138182151','266'),(267,'Alic Grimmett','97080 Blue Bill Park Crossing',20,'710654287','322672364','267'),(268,'Margaretta Greensides','1 Longview Parkway',195,'145022696','736111774','268'),(269,'Virgina Saffell','530 Memorial Street',209,'123837636','600558607','269'),(270,'Isabelita Brumble','34412 Ilene Point',125,'203795135','712664367','270'),(271,'Letta Bullent','8258 Gateway Trail',144,'690279786','486188536','271'),(272,'Vikky Jaquest','7 Fairview Hill',108,'109082302','295502707','272'),(273,'Staci Durn','300 Village Court',141,'294363425','627543850','273'),(274,'Legra Ingerson','3498 Vernon Crossing',253,'230365137','145927501','274'),(275,'Millisent Seater','291 Homewood Lane',77,'490525555','193654862','275'),(276,'Jerrie Kalinsky','08717 Hudson Hill',39,'265867167','751627469','276'),(277,'Kissee Rosas','8051 Kim Hill',206,'555010897','492715185','277'),(278,'Lusa Fraschetti','40666 Norway Maple Junction',92,'646962950','634269440','278'),(279,'Rosalia Hendrix','5953 Riverside Street',198,'672297109','276585497','279'),(280,'Kelci Townsend','6163 Rusk Trail',98,'785253098','131101034','280'),(281,'Donnell Bestwall','11217 Harper Point',74,'526194670','448698226','281'),(282,'Romona Carnell','5 Novick Crossing',129,'608738802','434485364','282'),(283,'Jemmie Cockarill','14 Golf View Drive',170,'271564959','337794978','283'),(284,'Simona Rosetti','7 Linden Center',31,'410767885','430368456','284'),(285,'Blake Bouchard','9 Scott Circle',10,'269786942','618309301','285'),(286,'Curtis Aberkirder','826 Vera Trail',54,'509112786','718516394','286'),(287,'Olag McGonagle','78 Victoria Avenue',255,'274770890','631116986','287'),(288,'Joachim Feares','6 Garrison Hill',169,'467390439','192571358','288'),(289,'Lynn Biasi','09571 Sheridan Crossing',63,'118014725','731034052','289'),(290,'Lyndsie Coughtrey','911 Green Court',60,'114981682','566943305','290'),(291,'Augustine Climson','1926 Scoville Place',236,'317272886','332631220','291'),(292,'Nickolas Traynor','43068 Waywood Lane',11,'664632382','541529786','292'),(293,'Christophe Eteen','0605 Anzinger Parkway',167,'354855342','492581269','293'),(294,'Maisie Hanster','36 Rockefeller Lane',86,'553249888','665702470','294'),(295,'Paige Bourner','770 Donald Point',52,'736854645','250501196','295'),(296,'Yalonda Pavie','040 American Point',260,'342573324','295007267','296'),(297,'Wynny Parmeter','423 Kenwood Street',76,'260467542','444374548','297'),(298,'Goldy MacClenan','164 Crescent Oaks Trail',99,'409450713','200640099','298'),(299,'Carmelia Lathee','1 Roth Crossing',228,'505141531','796752002','299'),(300,'Rosmunda Christofol','1343 Bobwhite Circle',146,'450156190','217893436','300'),(301,'Omero Wedgwood','4 Havey Trail',59,'334147566','490827822','301'),(302,'Brinna Vinsen','90705 Spenser Street',130,'294248575','392074547','302'),(303,'Kameko Morais','4886 Sunbrook Place',207,'367268087','116810275','303'),(304,'Jorrie Tristram','8609 Carberry Junction',36,'226037308','139784739','304'),(305,'Jocelyn Brownlie','3992 Sherman Pass',68,'499566249','415440113','305'),(306,'Marsh Whittlesee','4975 Schlimgen Terrace',194,'670615918','645855598','306'),(307,'Adrea Van Waadenburg','1329 Westport Terrace',246,'629415975','702927480','307'),(308,'Paulita Karlolczak','547 Dayton Road',226,'166938626','110510107','308'),(309,'Margarete Lilford','70995 Reindahl Avenue',108,'679664453','494308780','309'),(310,'Karlik Guille','89566 Green Ridge Center',247,'145272947','515004784','310'),(311,'Parrnell Witchalls','5222 Bartelt Park',126,'208914722','747105473','311'),(312,'Mitchell Spadeck','66625 Mallard Road',172,'358872974','552275462','312'),(313,'Theadora Bradder','74554 Westerfield Park',139,'450942791','703987995','313'),(314,'Tanner Renzini','33 Oak Point',23,'478912680','421926233','314'),(315,'Tansy Leneve','84 Trailsway Drive',93,'697091598','302946041','315'),(316,'Ring Daventry','8 Trailsway Center',23,'300229723','411267550','316'),(317,'Keenan Wimmers','7 Quincy Place',172,'713393517','345844982','317'),(318,'Philipa Dello','6433 Hermina Drive',216,'778835537','214100299','318'),(319,'Phoebe Conneely','3 Armistice Street',3,'260885844','516403680','319'),(320,'Merv Frier','103 Dorton Alley',136,'492140657','563103498','320'),(321,'Korrie Dmitrichenko','3 Killdeer Trail',27,'314708856','184724304','321'),(322,'Osgood Pala','0 Manley Court',73,'316843264','328195334','322'),(323,'Collette Maclaine','20619 Crownhardt Park',225,'219156179','550316813','323'),(324,'Chrissy Serraillier','0736 Norway Maple Drive',81,'520133297','182038369','324'),(325,'Kordula Adamiec','6308 Westend Junction',199,'143426767','522995958','325'),(326,'Marilin Tungay','70166 Crownhardt Point',204,'368258530','703727217','326'),(327,'Gino Marvelley','434 Green Ridge Junction',33,'160612011','305095309','327'),(328,'Leanna Shwenn','9 5th Hill',137,'239318618','591111134','328'),(329,'Pebrook Klasen','8 Westerfield Court',195,'705028164','104917332','329'),(330,'Leoline Linskey','522 Logan Plaza',232,'109382562','735496119','330'),(331,'Blithe Whitrod','284 Holy Cross Parkway',39,'752898515','739622218','331'),(332,'Garik Howlings','0 Tomscot Avenue',7,'460963922','615443415','332'),(333,'Rutherford Wiles','6 Riverside Road',227,'551080849','552430709','333'),(334,'Rozelle Pennicott','0 Waubesa Alley',213,'781424285','224544296','334'),(335,'Edita Muccino','29544 Stuart Road',60,'500551500','491731542','335'),(336,'Cookie Garlee','212 Norway Maple Terrace',83,'729732897','594150705','336'),(337,'Wren Bilney','94 Fieldstone Hill',125,'515747000','523887342','337'),(338,'Truda O\'Quin','93480 Trailsway Alley',52,'344601189','191591763','338'),(339,'Jane Domenichelli','0469 Bayside Place',208,'213406342','712471555','339'),(340,'Lanie Haigh','464 Wayridge Alley',8,'469671027','589876543','340'),(341,'Ada Denecamp','3 Mifflin Circle',130,'699182456','378585452','341'),(342,'Camella Joerning','6835 Algoma Alley',89,'426388008','152625763','342'),(343,'Nancee Helversen','62428 Atwood Street',182,'102642135','459573405','343'),(344,'Constantin Josephsen','04 Mallard Avenue',53,'559948157','125080083','344'),(345,'Rabbi Bentinck','1 Coleman Place',195,'562070536','745905814','345'),(346,'Rawley Clampton','29588 Valley Edge Court',21,'223902125','186825261','346'),(347,'Jessa Gouinlock','45200 Eastlawn Pass',120,'415875194','525575056','347'),(348,'Lindsay Coombs','8 Sage Alley',72,'474164243','123854862','348'),(349,'Deni Bartels-Ellis','098 Hermina Crossing',18,'259383427','310544456','349'),(350,'Brooke Duddan','5 Vidon Point',7,'577966238','270835878','350'),(351,'Keefe Sokale','006 Porter Road',250,'691916992','202920515','351'),(352,'Moyna Pedycan','1 Moose Plaza',148,'157941903','303095701','352'),(353,'Dimitry Tretwell','25 Blackbird Hill',232,'542089116','776499425','353'),(354,'Berte Stebbings','10 Barby Street',56,'159876583','344669788','354'),(355,'Hector Aguirrezabal','0 Briar Crest Trail',215,'622576169','635966406','355'),(356,'Kessia Mayoh','6 Goodland Plaza',84,'134226838','593975960','356'),(357,'Keeley Stoppard','9169 Talisman Road',12,'557742647','343129690','357'),(358,'Filbert Cheavin','691 Hoepker Lane',122,'330042066','654365316','358'),(359,'Simmonds Nice','5 Beilfuss Park',249,'677481178','449237159','359'),(360,'Mireielle Vasenkov','43 Summer Ridge Court',33,'449374872','540127151','360'),(361,'Orel Valiant','1 Pine View Way',212,'500515893','226504943','361'),(362,'Gray Steed','3 Fremont Parkway',260,'526901357','455021289','362'),(363,'Bettina Weare','1950 Colorado Center',173,'245653979','370570289','363'),(364,'Alana Mewe','5334 Hanover Point',79,'130939009','743678640','364'),(365,'Amandy Hugle','08 Moose Street',56,'187216017','379984988','365'),(366,'Genevieve D\'Costa','79573 Truax Trail',95,'491731605','111992248','366'),(367,'Joel Gilson','57015 Dovetail Alley',71,'662541861','631578924','367'),(368,'Quincey Daouze','5149 Evergreen Alley',239,'594553135','348711527','368'),(369,'Britni Ferrucci','440 Columbus Pass',155,'558332058','571521756','369'),(370,'Joyan Sawbridge','038 Northwestern Street',237,'263905070','144401790','370'),(371,'Bonnie Bowlesworth','0265 Springs Crossing',204,'526826151','172059985','371'),(372,'Luella Crompton','93 Roth Pass',138,'706513315','147690832','372'),(373,'Hoebart Joderli','96 Delladonna Point',64,'269167104','508162660','373'),(374,'Cornelia Timberlake','45 Reindahl Plaza',235,'501950595','485040988','374'),(375,'Denice Stallybrass','4262 Prairieview Trail',106,'113069962','600840142','375'),(376,'Skippie Lafflin','09 5th Parkway',109,'555201632','551829743','376'),(377,'Holt Prier','876 Calypso Point',146,'586247735','689784235','377'),(378,'Sammy Isley','1015 Meadow Vale Crossing',137,'660040401','510118251','378'),(379,'Gabriello Addie','790 Mcbride Lane',213,'192879692','494025702','379'),(380,'Linda Ebi','5357 Emmet Drive',163,'157310532','593111727','380'),(381,'Tirrell Philipsen','33 Tomscot Street',32,'603456971','640079758','381'),(382,'Marven Grabiec','4969 Hoffman Way',15,'111044066','328878976','382'),(383,'Ulla Hamshaw','3658 Heath Terrace',43,'527851820','256470869','383'),(384,'David MacAdie','726 Farmco Alley',53,'171084204','736280881','384'),(385,'Emmie Featonby','1665 Eagle Crest Pass',143,'639607017','667242715','385'),(386,'Hedda Bartles','4848 Havey Terrace',81,'584277812','789457518','386'),(387,'Nani Loffel','23 Kedzie Hill',134,'125296191','466857376','387'),(388,'Dominica Bandey','94986 Blue Bill Park Point',175,'114480896','389174143','388'),(389,'Kathie Wynch','11 Maywood Drive',3,'247192964','308304707','389'),(390,'Pavlov Rutter','64 Towne Court',40,'658210951','383225129','390'),(391,'Lorrin Runcieman','3 Crownhardt Junction',78,'352591177','306799431','391'),(392,'Meagan Quarles','85456 Walton Trail',199,'582067234','128908805','392'),(393,'Chad Ipgrave','039 Mariners Cove Circle',250,'669921309','237810987','393'),(394,'Genia Brissard','36886 Harbort Hill',71,'716886258','492582677','394'),(395,'Enoch Gatecliffe','858 Packers Drive',39,'663220072','585049490','395'),(396,'Beth Klosa','2532 Northview Drive',26,'136085932','169750724','396'),(397,'Biddy Garbott','3 Parkside Junction',134,'131698190','227099927','397'),(398,'Cami Whitney','56982 Charing Cross Pass',231,'428878480','625842845','398'),(399,'Lothaire Keays','970 Debra Alley',133,'632182473','295668057','399'),(400,'Pace Townley','66358 Mitchell Court',262,'610690722','420141941','400'),(401,'Jereme Abells','801 Sage Avenue',171,'363080791','625852883','401'),(402,'Rana De Caville','78952 Coleman Road',153,'613130669','555290749','402'),(403,'Teddie Dyne','31 Valley Edge Drive',36,'345909102','229071967','403'),(404,'Meredeth Pinkard','207 Jana Junction',196,'721148760','524255267','404'),(405,'Hyacinthia Sleney','40056 Lunder Parkway',125,'557860302','724746714','405'),(406,'Maura Lindborg','97180 Holmberg Parkway',71,'240188644','306532855','406'),(407,'Afton Lascelles','6940 Mockingbird Center',171,'678742365','507777893','407'),(408,'Celine Fortie','7622 Swallow Park',168,'273058258','661717016','408'),(409,'Nathanael Poynser','62 Caliangt Court',143,'721833642','262255107','409'),(410,'Kamila Dorken','35436 Vidon Place',130,'723520466','401378106','410'),(411,'Leighton Lennon','655 Rigney Way',241,'215495331','611721891','411'),(412,'Adiana Ridd','91 Barnett Road',210,'797775413','703330080','412'),(413,'Judi Fossick','0390 Mockingbird Road',36,'320889307','721351877','413'),(414,'Edeline Haxbie','5009 Merrick Hill',92,'414281022','790647580','414'),(415,'Ruperto Gimlet','694 Blue Bill Park Point',152,'717824784','677849064','415'),(416,'Jodi Friday','9 Delladonna Avenue',259,'647290028','746509932','416'),(417,'Gerta Colthurst','58972 Memorial Street',159,'518810125','204387235','417'),(418,'Corrianne Belfitt','55840 Pine View Street',40,'735391138','660366338','418'),(419,'Antony Labusch','669 Eastlawn Hill',145,'506554030','633313985','419'),(420,'Granny Richardes','2583 Lawn Junction',155,'291887129','782753531','420'),(421,'Cullie Melan','9 Esch Place',129,'384382163','748181471','421'),(422,'Joanna Siene','6304 Waubesa Place',175,'676731435','555766799','422'),(423,'Kimble Hanniger','9 Dunning Place',204,'586542066','415752762','423'),(424,'Llewellyn Augustin','8 South Street',5,'611615163','264054872','424'),(425,'Alexio Stockill','39 Bultman Junction',223,'428358394','202951158','425'),(426,'Jacquenetta Jesse','02832 Thackeray Hill',250,'228878735','665851087','426'),(427,'Hulda Baggelley','67 Alpine Hill',109,'224316307','564492730','427'),(428,'Teresina Cund','995 Buena Vista Drive',161,'445969925','358573266','428'),(429,'Eunice Burdis','4127 Anzinger Place',260,'602954412','151251792','429'),(430,'Morganica Redmain','2707 Londonderry Lane',260,'325671611','681816099','430'),(431,'Blakelee Stollard','73 Memorial Pass',77,'482218805','341365755','431'),(432,'Maury Varley','0509 Namekagon Park',72,'352169503','605415792','432'),(433,'Adriana Peakman','532 Little Fleur Junction',93,'148525871','504614542','433'),(434,'Baird Mazzilli','89013 Norway Maple Crossing',74,'370386409','534798726','434'),(435,'Bobinette Band','0827 Oriole Junction',186,'664736543','241371313','435'),(436,'Elane Bartolomeo','7 Havey Pass',13,'187475319','283362337','436'),(437,'Ellswerth Whitters','09240 Merry Junction',78,'731595375','366631395','437'),(438,'Eloisa Beazley','821 Burrows Pass',3,'283702402','113073945','438'),(439,'Perri Reeme','75830 Jana Parkway',145,'537710036','542046606','439'),(440,'Ephraim Hubeaux','07837 Logan Hill',179,'797997288','717728898','440'),(441,'Alta Minchinden','074 Sunbrook Parkway',179,'719833983','768999322','441'),(442,'Gerta Aslen','8761 Stoughton Court',152,'261060277','224296702','442'),(443,'Duff Geard','83 Homewood Plaza',66,'226194834','728648750','443'),(444,'Russell Burrage','4 Pennsylvania Center',202,'212930490','598317396','444'),(445,'Melvin Griss','208 Sachs Plaza',164,'211332617','579046295','445'),(446,'Si Rosewell','94 Gateway Place',95,'479007797','795057420','446'),(447,'Sara-ann Minchinton','24166 Rutledge Junction',82,'574411058','512625286','447'),(448,'Christye Vayne','131 La Follette Plaza',169,'436986068','263805373','448'),(449,'Ted Stranks','19 Cottonwood Road',123,'364766445','498041314','449'),(450,'Mame Conring','36330 Derek Way',113,'669133397','711657579','450'),(451,'Fara Raund','399 Debs Pass',249,'447395509','215311273','451'),(452,'Aindrea Coghill','560 Petterle Avenue',153,'501885458','182879340','452'),(453,'Ramon Ivakin','660 Weeping Birch Trail',153,'776666579','792386996','453'),(454,'Deanne Truce','47076 Elmside Road',87,'192981587','393855690','454'),(455,'Cissiee Ianinotti','78 Old Shore Alley',95,'114825449','191475876','455'),(456,'Werner Hixson','56 Kinsman Alley',28,'674004679','652478186','456'),(457,'Woodrow Feehily','8 Mcbride Plaza',215,'100511306','690325607','457'),(458,'Susannah Bitten','25351 Lotheville Road',155,'710960956','125608034','458'),(459,'Ronna Mourant','39 Mcguire Street',140,'794777491','360202833','459'),(460,'Meryl Zute','347 Hoffman Plaza',213,'697430652','635030760','460'),(461,'Viv Babidge','52 Hanson Alley',142,'768461306','456105544','461'),(462,'Melvyn Himpson','13197 Jenifer Junction',100,'402044650','343795249','462'),(463,'Angelika McGenn','29810 Farragut Plaza',227,'198760386','707720521','463'),(464,'Tomaso Schutter','6556 Badeau Crossing',176,'487327893','244585530','464'),(465,'Milka Ord','89609 Tomscot Junction',238,'336188036','231881143','465'),(466,'Lorette Bras','90 Clyde Gallagher Parkway',132,'391445385','629925084','466'),(467,'Ned McIlhone','693 Fieldstone Road',61,'117574633','567977240','467'),(468,'Luciana Wybrew','532 Texas Parkway',233,'283642290','149187208','468'),(469,'Julietta Kilmartin','56 Ridgeway Alley',163,'660387905','382103881','469'),(470,'Bo Chippendale','73539 Service Crossing',139,'140793629','394446898','470'),(471,'Constantina Dahlborg','772 Hooker Lane',158,'788214945','692793007','471'),(472,'Bern Dzeniskevich','68104 Fuller Way',136,'510212910','136542624','472'),(473,'Abeu Luparti','339 Briar Crest Junction',10,'782148653','155415835','473'),(474,'Hart Le Port','35776 Elgar Park',98,'271959218','121431429','474'),(475,'Delcine Brouncker','197 Caliangt Court',195,'196478868','486144020','475'),(476,'Joly Leatherland','576 Crescent Oaks Lane',220,'492937443','413847487','476'),(477,'Martica Rosell','8588 Bonner Place',75,'420337241','356802534','477'),(478,'Elroy Clarae','631 Artisan Junction',204,'691203066','329662185','478'),(479,'Nanice Grzegorek','1194 Mosinee Hill',236,'659432326','765993719','479'),(480,'Tabatha Moniker','3 Hallows Parkway',114,'575337501','549358321','480'),(481,'Orazio Sprowson','83 Jackson Avenue',146,'112514723','651913173','481'),(482,'Dennie Gibbens','029 Ridgeway Point',234,'767147862','761324805','482'),(483,'Brianna Morston','45 Menomonie Parkway',42,'585769458','197404352','483'),(484,'Hamilton Keslake','520 Twin Pines Terrace',29,'724805841','764643771','484'),(485,'Cleopatra Michurin','99943 Gateway Point',20,'250126672','185330982','485'),(486,'Colleen Yukhtin','9894 Reindahl Plaza',100,'244796829','632308902','486'),(487,'Gustave Cominoli','043 North Crossing',78,'492296184','388939943','487'),(488,'Janine Cominoli','0972 Karstens Hill',157,'348866819','246581960','488'),(489,'Efren Bradlaugh','312 Tennessee Way',218,'314576002','115136241','489'),(490,'Farrell Coomber','891 Elmside Lane',103,'466977370','467902738','490'),(491,'Maisie Licciardiello','82352 Washington Parkway',32,'353355232','342878594','491'),(492,'Carolyne Sterman','44 Sloan Park',10,'427612674','514686416','492'),(493,'Dale Bruineman','2 Helena Hill',219,'307906065','738964263','493'),(494,'Fair Focke','6 Dryden Center',183,'201529375','568663621','494'),(495,'Shelba Gobel','21 School Plaza',59,'526078052','708288965','495'),(496,'Adelaida Bentje','003 Nelson Hill',201,'624471209','186156937','496'),(497,'Celesta Mahody','3 Thackeray Center',247,'177360611','463356000','497'),(498,'Engelbert Maystone','40545 Golf Course Place',57,'505358533','541589490','498'),(499,'Maxy Rubinowitz','2986 Rockefeller Plaza',243,'543317417','359130820','499'),(500,'Giovanni Lorenzo','290 Eagle Crest Avenue',6,'274083152','321957302','500');
/*!40000 ALTER TABLE `maestro_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_insumo`
--

DROP TABLE IF EXISTS `maestro_insumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_insumo` (
  `idinsumo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(300) DEFAULT '',
  `unidad` char(30) DEFAULT NULL,
  PRIMARY KEY (`idinsumo`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_insumo`
--

LOCK TABLES `maestro_insumo` WRITE;
/*!40000 ALTER TABLE `maestro_insumo` DISABLE KEYS */;
INSERT INTO `maestro_insumo` VALUES (1,'Amitriptyline Hydrochloride','unidad'),(2,'Chlordiazepoxide Hydrochloride','unidad'),(3,'Petrolatum','unidad'),(4,'Simethicone','unidad'),(5,'Aluminum zirconium tetrachlorohydrex gly','unidad'),(6,'Levothyroxine Sodium','unidad'),(7,'tolazamide','unidad'),(8,'Ibuprofen','unidad'),(9,'Antiseptic handwash','unidad'),(10,'Hydralazine Hydrochloride','unidad'),(11,'Olanzapine','unidad'),(12,'Urea','unidad'),(13,'CALCIUM FLUORIDE','unidad'),(14,'hypromellose','unidad'),(15,'Menthol and Methyl Salicylate','unidad'),(16,'metoprolol succinate and hydrochlorothiazide','unidad'),(17,'INDOMETHACIN','unidad'),(18,'Titanium Dioxide and Zinc Oxide','unidad'),(19,'Triclosan','unidad'),(20,'Lamotrigine','unidad'),(21,'Dandelion','unidad'),(22,'DEXTROMETHORPHAN HYDROBROMIDE and DOXYLAMINE SUCCINATE','unidad'),(23,'White Mulberry','unidad'),(24,'Naratriptan','unidad'),(25,'Oxygen','unidad'),(26,'OCTINOXATE, OCTISALATE','unidad'),(27,'menthol','unidad'),(28,'Ceftriaxone Sodium','unidad'),(29,'Diclofenac Sodium','unidad'),(30,'butalbital, acetaminophen and caffeine','unidad'),(31,'ARIPIPRAZOLE','unidad'),(32,'ALCOHOL','unidad'),(33,'Ammonium Lactate','unidad'),(34,'CHLORDIAZEPOXIDE HYDROCHLORIDE and CLIDINIUM BROMIDE','unidad'),(35,'Ibuprofen','unidad'),(36,'Zolmitriptan','unidad'),(37,'Olanzapine','unidad'),(38,'Naproxen sodium','unidad'),(39,'carboxymethylcellulose sodium','unidad'),(40,'SERTRALINE HYDROCHLORIDE','unidad'),(41,'bacitracin, neomycin, polymyxin B','unidad'),(42,'Lansoprazole','unidad'),(43,'diltiazem','unidad'),(44,'Estradiol','unidad'),(45,'Carbidopa and levodopa','unidad'),(46,'Agnus castus e sem. 4','unidad'),(47,'TITANIUM DIOXIDE','unidad'),(48,'Phenol','unidad'),(49,'ALCOHOL','unidad'),(50,'benztropine mesylate','unidad'),(51,'QUETIAPINE FUMARATE','unidad'),(52,'Avobenzone, Octisalate, Octocrylene and Oxybenzone','unidad'),(53,'Titanium Dioxide and Octinoxate','unidad'),(54,'Ethyl Macadamiate','unidad'),(55,'OCTINOXATE, TITANIUM DIOXIDE, and ZINC OXIDE','unidad'),(56,'Benzalkonium chloride','unidad'),(57,'nicotine','unidad'),(58,'OCTINOXATE, OCTOCRYLENE, AVOBENZONE','unidad'),(59,'Nortriptyline Hydrochloride','unidad'),(60,'Avobenzone, Octinoxate, Octisalate, Oxybenzone','unidad'),(61,'Piroxicam','unidad'),(62,'Brompheniramine maleate, Dextromethorphan HBr, Phenylephrine HCl','unidad'),(63,'Chlorzoxazone','unidad'),(64,'MENTHOL','unidad'),(65,'Quetiapine fumarate','unidad'),(66,'Rosuvastatin calcium','unidad'),(67,'HOMOSALATE, OCTISALATE, OXYBENZONE, AVOBENZONE, OCTOCRYLENE','unidad'),(68,'Aconitum napellus, Hypericum perforatum, Lycopodium clavatum, Phosphorus, Rhus toxicodendron, Secale cornutum','unidad'),(69,'Cyclobenzaprine Hydrochloride','unidad'),(70,'Phenylephrine Hydrochloride','unidad'),(71,'Metronidazole','unidad'),(72,'Dextromethorphan HBr, Guaifenesin','unidad'),(73,'Cholecalciferol, Ascorbic Acid, Folic Acid, Thiamin, Riboflavin, Niacin, Pantothenic Acid, Pyridoxine HCl, Biotin, Cyanocobalamin','unidad'),(74,'Dill','unidad'),(75,'Lycopodium clavatum, Pulsatilla,','unidad'),(76,'PredniSONE','unidad'),(77,'Midazolam','unidad'),(78,'Ethyl Alcohol','unidad'),(79,'Divalproex Sodium','unidad'),(80,'Butalbital, Acetaminophen, Caffeine, and Codeine Phosphate','unidad'),(81,'Pelargonium sidoides','unidad'),(82,'Octinoxate and Titanium dioxide','unidad'),(83,'warfarin sodium','unidad'),(84,'Metoclopramide Hydrochloride','unidad'),(85,'AVOBENZONE, OCTINOXATE, OCTISALATE, OXYBENZONE','unidad'),(86,'Triclosan','unidad'),(87,'Sotalol Hydrochloride','unidad'),(88,'Fentanyl','unidad'),(89,'von Willebrand Factor/Coagulation Factor VIII Complex (Human)','unidad'),(90,'Angelica Archangelica, Caulophyllum Thalictroides, Disoscorea Villosa, Oophorinum (Suis) Thyroidinum (Suis), Uterus (Suis), Zincum Gluconicum','unidad'),(91,'White Petrolatum','unidad'),(92,'Quetiapine fumarate','unidad'),(93,'DOXOrubicin Hydrochloride','unidad'),(94,'White Petrolatum','unidad'),(95,'rifampin, isoniazid and pyrazinamide','unidad'),(96,'Bismuth Subsalicylate','unidad'),(97,'Ranitidine','unidad'),(98,'HYDROQUINONE','unidad'),(99,'mesna','unidad'),(100,'California Live Oak','unidad');
/*!40000 ALTER TABLE `maestro_insumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_municipio`
--

DROP TABLE IF EXISTS `maestro_municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_municipio` (
  `idmunicipio` int(11) NOT NULL AUTO_INCREMENT,
  `departamento` varchar(60) NOT NULL DEFAULT '',
  `nombre` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`idmunicipio`),
  KEY `fk_maestromunicipio_maestrodepartamento` (`departamento`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_municipio`
--

LOCK TABLES `maestro_municipio` WRITE;
/*!40000 ALTER TABLE `maestro_municipio` DISABLE KEYS */;
INSERT INTO `maestro_municipio` VALUES (1,'Sonsonate','Acajutla'),(2,'Chalatenango','Agua Caliente'),(3,'San Salvador','Aguilares'),(4,'Ahuachapan','Ahuachapán'),(5,'Usulutan','Alegría'),(6,'La Union','Anamorós'),(7,'La Libertad','Antiguo Cuscatlán'),(8,'Ahuachapan','Apaneca'),(9,'San Vicente','Apastepeque'),(10,'San Salvador','Apopa'),(11,'Morazan','Arambala'),(12,'Chalatenango','Arcatao'),(13,'Sonsonate','Armenia'),(14,'Ahuachapan','Atiquizaya'),(15,'San Salvador','Ayutuxtepeque'),(16,'Chalatenango','Azacualpa'),(17,'Usulutan','Berlín'),(18,'La Union','Bolívar'),(19,'Morazan','Cacaopera'),(20,'Usulutan','California'),(21,'Sonsonate','Caluco'),(22,'Cuscatlan','Candelaria'),(23,'Santa Ana','Candelaria de la Frontera'),(24,'San Miguel','Carolina'),(25,'Chalatenango','Chalatenango'),(26,'Santa Ana','Chalchuapa'),(27,'San Miguel','Chapeltique'),(28,'Morazan','Chilanga'),(29,'La Libertad','Chiltiupán'),(30,'San Miguel','Chinameca'),(31,'San Miguel','Chirilagua'),(32,'Cabañas','Cinquera'),(33,'Chalatenango','Citalá'),(34,'La Libertad','Ciudad Arce'),(35,'San Miguel','Ciudad Barrios'),(36,'Santa Ana','Coatepeque'),(37,'Cuscatlan','Cojutepeque'),(38,'La Libertad','Colón'),(39,'San Miguel','Comacarán'),(40,'Chalatenango','Comalapa'),(41,'La Libertad','Comasagua'),(42,'Usulutan','Concepción Batres'),(43,'Ahuachapan','Concepción de Ataco'),(44,'La Union','Concepción de Oriente'),(45,'Chalatenango','Concepción Quezaltepeque'),(46,'La Union','Conchagua'),(47,'Morazan','Corinto'),(48,'Sonsonate','Cuisnahuat'),(49,'San Salvador','Cuscatancingo'),(50,'La Paz','Cuyultitán'),(51,'San Salvador','Delgado'),(52,'Morazan','Delicias de Concepción'),(53,'Cabañas','Dolores'),(54,'Chalatenango','Dulce Nombre de María'),(55,'Cuscatlan','El Carmen'),(56,'La Union','El Carmen'),(57,'Chalatenango','El Carrizal'),(58,'Santa Ana','El Congo'),(59,'Morazan','El Divisadero'),(60,'San Salvador','El Paisnal'),(61,'Chalatenango','El Paraíso'),(62,'Santa Ana','El Porvenir'),(63,'Ahuachapan','El Refugio'),(64,'Cuscatlan','El Rosario'),(65,'Morazan','El Rosario'),(66,'La Paz','El Rosario'),(67,'La Union','El Sauce'),(68,'San Miguel','El Tránsito'),(69,'Usulutan','El Triunfo'),(70,'Usulutan','Ereguayquín'),(71,'Usulutan','Estanzuelas'),(72,'Cabañas','Guacotecti'),(73,'San Vicente','Guadalupe'),(74,'Morazan','Gualococti'),(75,'Morazan','Guatajiagua'),(76,'Ahuachapan','Guaymango'),(77,'San Salvador','Guazapa'),(78,'La Libertad','Huizúcar'),(79,'Cabañas','Ilobasco'),(80,'San Salvador','Ilopango'),(81,'La Union','Intipucá'),(82,'Sonsonate','Izalco'),(83,'La Libertad','Jayaque'),(84,'La Paz','Jerusalén'),(85,'La Libertad','Jicalapa'),(86,'Usulutan','Jiquilisco'),(87,'Morazan','Joateca'),(88,'Morazan','Jocoaitique'),(89,'Morazan','Jocoro'),(90,'Sonsonate','Juayúa'),(91,'Usulutan','Jucuapa'),(92,'Usulutan','Jucuarán'),(93,'Ahuachapan','Jujutla'),(94,'Cabañas','Jutiapa'),(95,'Chalatenango','La Laguna'),(96,'La Libertad','La Libertad'),(97,'Chalatenango','La Palma'),(98,'Chalatenango','La Reina'),(99,'Chalatenango','Las Vueltas'),(100,'La Union','La Unión'),(101,'La Union','Lislique'),(102,'San Miguel','Lolotique'),(103,'Morazan','Lolotiquillo'),(104,'Santa Ana','Masahuat'),(105,'Morazan','Meanguera'),(106,'La Union','Meanguera del Golfo'),(107,'San Salvador','Mejicanos'),(108,'La Paz','Mercedes La Ceiba'),(109,'Usulutan','Mercedes Umaña'),(110,'Santa Ana','Metapán'),(111,'San Miguel','Moncagua'),(112,'Cuscatlan','Monte San Juan'),(113,'Sonsonate','Nahuizalco'),(114,'Sonsonate','Nahulingo'),(115,'San Salvador','Nejapa'),(116,'Chalatenango','Nombre de Jesús'),(117,'Chalatenango','Nueva Concepción'),(118,'La Union','Nueva Esparta'),(119,'Usulutan','Nueva Granada'),(120,'San Miguel','Nueva Guadalupe'),(121,'La Libertad','Nueva San Salvador'),(122,'Chalatenango','Nueva Trinidad'),(123,'La Libertad','Nuevo Cuscatlán'),(124,'San Miguel','Nuevo Edén de San Juan'),(125,'Chalatenango','Ojos de Agua'),(126,'La Paz','Olocuilta'),(127,'La Libertad','Opico'),(128,'Cuscatlan','Oratorio de Concepción'),(129,'Morazan','Osicala'),(130,'Usulutan','Ozatlán'),(131,'San Salvador','Panchimalco'),(132,'La Paz','Paraíso de Osorio'),(133,'La Union','Pasaquina'),(134,'Morazan','Perquín'),(135,'La Union','Polorós'),(136,'Chalatenango','Potonico'),(137,'Usulutan','Puerto El Triunfo'),(138,'San Miguel','Quelepa'),(139,'La Libertad','Quezaltepeque'),(140,'San Salvador','Rosario de Mora'),(141,'La Libertad','Sacacoyo'),(142,'Sonsonate','Salcoatitán'),(143,'Usulutan','San Agustín'),(144,'La Union','San Alejo'),(145,'San Miguel','San Antonio'),(146,'Chalatenango','San Antonio de la Cruz'),(147,'Sonsonate','San Antonio del Monte'),(148,'Chalatenango','San Antonio Los Ranchos'),(149,'La Paz','San Antonio Masahuat'),(150,'Santa Ana','San Antonio Pajonal'),(151,'Cuscatlan','San Bartolomé Perulapía'),(152,'Usulutan','San Buenaventura'),(153,'Morazan','San Carlos'),(154,'San Vicente','San Cayetano Istepeque'),(155,'Cuscatlan','San Cristóbal'),(156,'Usulutan','San Dionisio'),(157,'La Paz','San Emigdio'),(158,'San Vicente','San Esteban Catarina'),(159,'Chalatenango','San Fernando'),(160,'Morazan','San Fernando'),(161,'La Paz','San Francisco Chinameca'),(162,'Morazan','San Francisco Gotera'),(163,'Usulutan','San Francisco Javier'),(164,'Chalatenango','San Francisco Lempa'),(165,'Ahuachapan','San Francisco Menéndez'),(166,'Chalatenango','San Francisco Morazán'),(167,'San Miguel','San Gerardo'),(168,'Chalatenango','San Ignacio'),(169,'San Vicente','San Ildefonso'),(170,'Cabañas','San Isidro'),(171,'Morazan','San Isidro'),(172,'Chalatenango','San Isidro Labrador'),(173,'San Miguel','San Jorge'),(174,'La Union','San José'),(175,'Chalatenango','San José Cancasque'),(176,'Cuscatlan','San José Guayabal'),(177,'Chalatenango','San José Las Flores'),(178,'La Libertad','San José Villanueva'),(179,'La Paz','San Juan Nonualco'),(180,'La Paz','San Juan Talpa'),(181,'La Paz','San Juan Tepezontes'),(182,'Sonsonate','San Julián'),(183,'Ahuachapan','San Lorenzo'),(184,'San Vicente','San Lorenzo'),(185,'San Miguel','San Luis de la Reina'),(186,'Chalatenango','San Luis del Carmen'),(187,'La Paz','San Luis La Herradura'),(188,'La Paz','San Luis Talpa'),(189,'San Salvador','San Marcos'),(190,'San Salvador','San Martín'),(191,'La Libertad','San Matías'),(192,'San Miguel','San Miguel'),(193,'Chalatenango','San Miguel de Mercedes'),(194,'La Paz','San Miguel Tepezontes'),(195,'La Libertad','San Pablo Tacachico'),(196,'La Paz','San Pedro Masahuat'),(197,'La Paz','San Pedro Nonualco'),(198,'Cuscatlan','San Pedro Perulapán'),(199,'Ahuachapan','San Pedro Puxtla'),(200,'Chalatenango','San Rafael'),(201,'San Miguel','San Rafael'),(202,'Cuscatlan','San Rafael Cedros'),(203,'La Paz','San Rafael Obrajuelo'),(204,'Cuscatlan','San Ramón'),(205,'San Salvador','San Salvador'),(206,'San Vicente','San Sebastián'),(207,'Santa Ana','San Sebastián Salitrillo'),(208,'Morazan','San Simón'),(209,'Santa Ana','Santa Ana'),(210,'Sonsonate','Santa Catarina Masahuat'),(211,'San Vicente','Santa Clara'),(212,'Cuscatlan','Santa Cruz Analquito'),(213,'Cuscatlan','Santa Cruz Michapa'),(214,'Usulutan','Santa Elena'),(215,'Sonsonate','Santa Isabel Ishuatán'),(216,'Usulutan','Santa María'),(217,'La Paz','Santa María Ostuma'),(218,'Chalatenango','Santa Rita'),(219,'La Union','Santa Rosa de Lima'),(220,'Santa Ana','Santa Rosa Guachipilín'),(221,'Santa Ana','Santiago de la Frontera'),(222,'Usulutan','Santiago de María'),(223,'La Paz','Santiago Nonualco'),(224,'San Salvador','Santiago Texacuangos'),(225,'Sonsonate','Santo Domingo'),(226,'San Vicente','Santo Domingo'),(227,'San Salvador','Santo Tomás'),(228,'San Vicente','San Vicente'),(229,'Morazan','Sensembra'),(230,'Cabañas','Sensuntepeque'),(231,'San Miguel','Sesori'),(232,'Morazan','Sociedad'),(233,'Sonsonate','Sonsonate'),(234,'Sonsonate','Sonzacate'),(235,'San Salvador','Soyapango'),(236,'Cuscatlan','Suchitoto'),(237,'Ahuachapan','Tacuba'),(238,'La Libertad','Talnique'),(239,'La Libertad','Tamanique'),(240,'La Paz','Tapalhuaca'),(241,'Usulutan','Tecapán'),(242,'San Vicente','Tecoluca'),(243,'Cabañas','Tejutepeque'),(244,'Chalatenango','Tejutla'),(245,'Cuscatlan','Tenancingo'),(246,'La Libertad','Teotepeque'),(247,'La Libertad','Tepecoyo'),(248,'San Vicente','Tepetitán'),(249,'Santa Ana','Texistepeque'),(250,'San Salvador','Tonacatepeque'),(251,'Morazan','Torola'),(252,'Ahuachapan','Turín'),(253,'San Miguel','Uluazapa'),(254,'Usulutan','Usulután'),(255,'San Vicente','Verapaz'),(256,'Cabañas','Victoria'),(257,'Morazan','Yamabal'),(258,'La Union','Yayantique'),(259,'Morazan','Yoloaiquín'),(260,'La Union','Yucuaiquín'),(261,'La Paz','Zacatecoluca'),(262,'La Libertad','Zaragoza');
/*!40000 ALTER TABLE `maestro_municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_proveedor`
--

DROP TABLE IF EXISTS `maestro_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_proveedor` (
  `idproveedor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL DEFAULT '',
  `direccion` varchar(300) NOT NULL DEFAULT '',
  `telefono` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`idproveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_proveedor`
--

LOCK TABLES `maestro_proveedor` WRITE;
/*!40000 ALTER TABLE `maestro_proveedor` DISABLE KEYS */;
INSERT INTO `maestro_proveedor` VALUES (1,'Super Selectos','San Salvador ','12345'),(2,'muebleria las victorias','ciudad delgado','121212'),(3,'Kwideo','70044 Arrowood Alley','746-693-1922'),(4,'Vinte','39 Crowley Way','413-666-5917'),(5,'Jaxnation','000 Shoshone Place','481-933-1162'),(6,'Realcube','2 Anthes Point','842-177-5929'),(7,'Skiptube','7412 Annamark Hill','886-406-7003'),(8,'Fatz','73 Waubesa Place','458-668-5776'),(9,'Browsecat','847 Ridgeway Hill','200-924-6107'),(10,'Avamm','587 Troy Plaza','787-703-5556'),(11,'Tanoodle','4 Center Trail','991-985-4268'),(12,'Skimia','3324 Ridgeway Pass','664-601-0214'),(13,'Dabshots','9089 Westport Circle','278-904-7141'),(14,'Gabvine','59 Roth Court','527-900-5624'),(15,'Centimia','9496 Steensland Alley','494-769-6805'),(16,'Jaxspan','32264 Westridge Point','579-298-6639'),(17,'Twitterbridge','65657 Mariners Cove Avenue','875-254-4463'),(18,'Pixope','73 Norway Maple Pass','134-815-1989'),(19,'Oyoyo','81930 7th Pass','920-951-9944'),(20,'Demizz','653 Sutherland Terrace','212-126-9522'),(21,'Jayo','6 8th Way','862-809-9170'),(22,'Mynte','8380 Prentice Court','967-134-4828'),(23,'Edgepulse','84086 Iowa Avenue','114-172-7976'),(24,'Eare','45 Gale Parkway','937-262-2659'),(25,'Bubblebox','0 Morningstar Lane','450-312-4344'),(26,'Mydeo','28 Morning Drive','849-928-0451'),(27,'Quinu','985 Westerfield Alley','644-783-5130'),(28,'Oozz','644 Kedzie Terrace','260-475-7495'),(29,'Vipe','0 Brickson Park Trail','448-666-2272'),(30,'Gabtune','93298 Moose Center','322-460-3800'),(31,'Realcube','8126 Grim Center','741-263-4410'),(32,'Centizu','631 Elmside Avenue','602-779-7501'),(33,'Cogilith','757 Maple Place','173-840-9836'),(34,'Edgeclub','7 Dawn Point','167-532-7853'),(35,'Trilia','2 Main Court','104-518-7540'),(36,'Blogtags','2286 Goodland Drive','590-163-1490'),(37,'Rhyloo','76164 Barnett Place','658-616-8774'),(38,'Rhyzio','9730 Bunker Hill Center','419-790-4246'),(39,'Tagtune','4981 Mcguire Avenue','429-233-7322'),(40,'Flipbug','2 Manitowish Way','368-278-3700'),(41,'Midel','77298 Hanson Trail','860-687-9537'),(42,'Divape','1 Moulton Place','260-982-3905'),(43,'Livetube','3359 Anhalt Place','173-783-8687'),(44,'Fiveclub','9545 Fair Oaks Avenue','181-503-6098'),(45,'Npath','1250 Northridge Way','601-907-1515'),(46,'Shuffledrive','6190 Carey Center','941-598-1991'),(47,'Abata','3479 Waywood Parkway','946-615-7131'),(48,'Trilith','4835 Jackson Pass','393-366-7726'),(49,'Viva','47 Center Lane','140-417-4803'),(50,'Thoughtworks','00 Warbler Parkway','607-636-8650'),(51,'Divanoodle','241 East Crossing','987-473-3828'),(52,'Meemm','35 Ohio Pass','173-654-9799'),(53,'Linklinks','95651 Elgar Alley','747-136-5061'),(54,'Skibox','95535 Homewood Alley','674-154-4424'),(55,'Innotype','1 Commercial Center','828-884-7690'),(56,'Dabtype','0085 Knutson Trail','972-485-2130'),(57,'Twinte','2 Talmadge Terrace','554-362-4348'),(58,'Oyope','314 Glendale Lane','486-511-5361'),(59,'Chatterbridge','0 Mallard Court','664-217-8898'),(60,'Topiclounge','98 Logan Point','393-468-5456'),(61,'Linkbuzz','80479 Acker Court','940-862-2914'),(62,'Divape','192 Scott Hill','482-802-3122'),(63,'Ooba','673 Sherman Place','236-980-1414'),(64,'Yambee','448 Lawn Junction','996-590-6492'),(65,'Brightbean','4336 Bartelt Drive','223-114-8089'),(66,'Dabfeed','74756 Dexter Court','481-628-6973'),(67,'Realpoint','9028 John Wall Junction','588-239-0830'),(68,'Innotype','226 Rockefeller Street','890-317-4488'),(69,'Voonte','6 Elka Junction','789-181-4589'),(70,'Chatterpoint','1283 Crowley Circle','522-306-1240'),(71,'Jabbersphere','2891 Petterle Point','819-337-7354'),(72,'Livefish','262 Florence Park','386-942-1330'),(73,'Thoughtworks','1863 Victoria Alley','603-149-2082'),(74,'Tazz','09 Golf View Junction','114-354-2685'),(75,'Skinte','4578 Cottonwood Crossing','713-473-6866'),(76,'Yodo','6 Schlimgen Alley','448-418-7953'),(77,'Podcat','3 Dixon Point','839-797-3933'),(78,'Vinte','002 Meadow Ridge Road','229-342-9237'),(79,'Browseblab','3 8th Center','423-436-7921'),(80,'Buzzshare','3 Aberg Court','319-685-4902'),(81,'Edgeblab','48884 Ilene Lane','164-813-6237'),(82,'Bubblebox','0268 Kipling Drive','142-222-2066'),(83,'Devcast','96097 Dawn Crossing','666-429-0906'),(84,'Livetube','31048 Nova Place','506-875-8778'),(85,'Skajo','2 Southridge Parkway','739-588-5886'),(86,'Buzzster','50746 Clemons Crossing','665-868-7565'),(87,'Devify','77 Butterfield Center','190-955-0609'),(88,'Oyoloo','345 Artisan Drive','713-163-0277'),(89,'Pixope','0 Muir Court','678-355-0006'),(90,'Eazzy','84 Pleasure Place','245-138-5060'),(91,'Shuffletag','16 John Wall Pass','154-189-1234'),(92,'Livefish','7841 Fulton Terrace','732-674-2540'),(93,'Roomm','36622 Summerview Drive','976-953-8131'),(94,'Aibox','5635 Mayfield Hill','953-256-2108'),(95,'Wikibox','91353 Garrison Lane','310-135-8943'),(96,'Gabcube','1731 Haas Lane','991-538-9859'),(97,'Quatz','99853 Lyons Crossing','748-496-0123'),(98,'Shufflester','51 Maple Wood Trail','949-992-7050'),(99,'Flashdog','84104 Sage Lane','441-908-9216'),(100,'Blogpad','78548 Cambridge Center','834-243-1624'),(101,'Livepath','76 Blackbird Alley','688-843-9507'),(102,'Innojam','6 Saint Paul Lane','616-716-0418'),(103,'Dabshots','4290 Eliot Court','803-966-4912'),(104,'Linktype','790 Talisman Terrace','769-167-8788'),(105,'Demizz','793 Hauk Drive','339-174-0169'),(106,'Thoughtbeat','230 Northport Road','321-318-8975'),(107,'Skyble','3650 Reindahl Road','273-564-0354'),(108,'Wordtune','1783 Petterle Parkway','900-670-1556'),(109,'Devpulse','49 Saint Paul Lane','422-945-6582'),(110,'Livetube','83 Hoard Drive','118-570-2321'),(111,'Kimia','9 Corben Hill','982-992-4893'),(112,'Minyx','46 Stoughton Road','143-440-5713'),(113,'Lazz','25549 Tony Alley','726-147-3430'),(114,'Oba','83181 Petterle Center','378-163-6630'),(115,'Quimm','35554 Hintze Pass','349-373-8430'),(116,'Dabjam','976 Reinke Avenue','722-294-2226'),(117,'LiveZ','617 Moland Parkway','847-911-1669'),(118,'Shufflester','9559 Jana Court','278-125-9171'),(119,'Innojam','429 Continental Road','528-619-9904'),(120,'Eidel','935 Starling Center','257-408-5095'),(121,'Twimm','71800 Elka Junction','130-664-7537'),(122,'Browsedrive','02698 Continental Drive','197-665-4319'),(123,'Reallinks','49870 Menomonie Street','813-560-8212'),(124,'Riffwire','2 Prairie Rose Alley','578-583-0973'),(125,'Yabox','931 Di Loreto Plaza','779-639-9750'),(126,'Jabberbean','06 Burning Wood Court','776-586-7983'),(127,'Trilith','389 Forest Dale Street','496-894-0432'),(128,'Gabtune','79673 Dayton Lane','206-436-6178'),(129,'Youfeed','2 Manley Street','761-404-0392'),(130,'Photobug','98 Service Lane','246-987-2751'),(131,'Vidoo','4 Gerald Way','495-604-6190'),(132,'Eidel','945 Del Mar Lane','489-248-9203'),(133,'Centimia','619 Alpine Trail','917-176-7278'),(134,'Camido','48787 Pankratz Pass','573-862-5416'),(135,'Skinder','806 Oneill Court','287-152-0099'),(136,'Youbridge','62 Blue Bill Park Drive','257-731-9065'),(137,'Rhycero','8890 Canary Parkway','200-835-8811'),(138,'Roodel','39946 Village Plaza','542-606-2122'),(139,'Feedfire','3 Nelson Place','678-630-2611'),(140,'Nlounge','85308 Grim Plaza','916-187-6678'),(141,'Browseblab','2 Merrick Plaza','149-696-7719'),(142,'Oba','30 Lien Plaza','281-473-2989'),(143,'Vimbo','8749 Hollow Ridge Park','164-657-7843'),(144,'Demimbu','1 Banding Road','519-411-7724'),(145,'Wikizz','73 6th Crossing','109-907-7468'),(146,'Jatri','626 Carey Avenue','638-880-8837'),(147,'Yacero','0 Ludington Circle','975-295-7468'),(148,'Gabvine','1807 Prairie Rose Pass','491-743-5369'),(149,'Dynabox','2 Clove Way','695-374-4946'),(150,'Yoveo','424 Beilfuss Drive','678-350-1576'),(151,'Yozio','62 Main Hill','905-408-7767'),(152,'Eidel','082 Eliot Terrace','546-632-0390'),(153,'Oyoyo','679 Mcguire Plaza','830-527-7700'),(154,'Digitube','42 Victoria Place','418-286-2359'),(155,'Riffwire','9401 Carioca Lane','872-194-0113'),(156,'Wordpedia','38 Riverside Street','290-378-1303'),(157,'Gevee','535 3rd Pass','525-204-5895'),(158,'Zoomlounge','7 Glendale Point','811-272-4602'),(159,'Roombo','4 Farwell Circle','880-383-3926'),(160,'Quimm','90139 Esch Parkway','629-592-9045'),(161,'Voomm','443 Quincy Point','173-964-0405'),(162,'Jetpulse','49 Sheridan Trail','549-707-0785'),(163,'Browsedrive','2989 Walton Place','322-356-0488'),(164,'Centimia','37646 Fallview Trail','597-614-0128'),(165,'Abatz','4357 Transport Alley','954-540-6212'),(166,'Wikibox','59272 Debs Trail','432-507-1828'),(167,'Rhycero','200 Rockefeller Plaza','284-518-7850'),(168,'Jaxworks','8 Annamark Court','688-614-0796'),(169,'Fadeo','0393 Crescent Oaks Road','397-289-5683'),(170,'Yadel','242 Swallow Avenue','793-869-3671'),(171,'Cogilith','3134 Mockingbird Terrace','524-890-2983'),(172,'Zoombox','61402 Wayridge Parkway','246-313-7099'),(173,'Dabjam','15746 Farmco Pass','133-371-2471'),(174,'Jazzy','7 Scofield Street','281-176-2281'),(175,'Chatterpoint','08 Fisk Pass','274-497-6674'),(176,'Voonyx','99 Sherman Place','429-191-5542'),(177,'Tambee','3 Holy Cross Pass','946-127-4424'),(178,'Skidoo','696 Jenifer Plaza','241-732-8285'),(179,'Ailane','5 Mallory Pass','952-930-4865'),(180,'Blognation','1 Brickson Park Drive','447-161-1653'),(181,'Demivee','8 Sugar Trail','924-548-0063'),(182,'Devshare','14497 Debs Alley','273-656-3722'),(183,'Dabfeed','03 Mariners Cove Lane','214-455-5355'),(184,'Blognation','697 Sunfield Lane','558-659-9491'),(185,'Meevee','8975 Duke Hill','755-269-8737'),(186,'Bluezoom','681 Canary Road','150-591-2336'),(187,'Meevee','03085 Jenna Point','469-468-7916'),(188,'Rhyzio','2 Thackeray Junction','791-126-6838'),(189,'Fadeo','718 Lyons Court','213-701-1158'),(190,'Demivee','859 Sachtjen Center','579-880-5148'),(191,'Cogidoo','83638 Lukken Terrace','641-553-7473'),(192,'Eazzy','2582 Kinsman Pass','349-969-0830'),(193,'Browsezoom','616 Morningstar Place','849-944-0839'),(194,'Avamba','36 Transport Street','661-512-5897'),(195,'Thoughtblab','030 Swallow Alley','918-208-5905'),(196,'Skippad','50295 Westridge Circle','576-934-5179'),(197,'Thoughtstorm','5973 Victoria Drive','755-277-2058'),(198,'Yodo','91588 Cordelia Crossing','755-961-7643'),(199,'Topicblab','27 Grasskamp Center','502-225-4570'),(200,'Voolia','745 Eastwood Lane','212-267-7642');
/*!40000 ALTER TABLE `maestro_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_servicio`
--

DROP TABLE IF EXISTS `maestro_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_servicio` (
  `idservicio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(300) DEFAULT '',
  `precio` double(12,2) DEFAULT NULL,
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_servicio`
--

LOCK TABLES `maestro_servicio` WRITE;
/*!40000 ALTER TABLE `maestro_servicio` DISABLE KEYS */;
INSERT INTO `maestro_servicio` VALUES (1,'LAVADO CAMISA',2.50,1,1,1,1),(2,'LAVADO PANTALON',2.00,0,1,1,1),(3,'LAVADO PELUCHE',3.25,1,1,1,1),(4,'LAVADO FORROS',3.00,0,0,0,0),(5,'CAMBIO ZUELA',3.00,1,0,0,0);
/*!40000 ALTER TABLE `maestro_servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_tienda`
--

DROP TABLE IF EXISTS `maestro_tienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tienda` (
  `idtienda` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `ubicacion` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`idtienda`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_tienda`
--

LOCK TABLES `maestro_tienda` WRITE;
/*!40000 ALTER TABLE `maestro_tienda` DISABLE KEYS */;
INSERT INTO `maestro_tienda` VALUES (1,'Masferrer','La Escalon'),(2,'Calle Arce','San Salvador'),(3,'San Jose','Olocuilta');
/*!40000 ALTER TABLE `maestro_tienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_tipo_movimiento`
--

DROP TABLE IF EXISTS `maestro_tipo_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tipo_movimiento` (
  `idtipomovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL DEFAULT '',
  `operacion` char(1) DEFAULT '',
  PRIMARY KEY (`idtipomovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_tipo_movimiento`
--

LOCK TABLES `maestro_tipo_movimiento` WRITE;
/*!40000 ALTER TABLE `maestro_tipo_movimiento` DISABLE KEYS */;
INSERT INTO `maestro_tipo_movimiento` VALUES (1,'Ingreso','1'),(2,'Egreso','0');
/*!40000 ALTER TABLE `maestro_tipo_movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_tipo_pago`
--

DROP TABLE IF EXISTS `maestro_tipo_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_tipo_pago` (
  `idtipopago` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) NOT NULL,
  PRIMARY KEY (`idtipopago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_tipo_pago`
--

LOCK TABLES `maestro_tipo_pago` WRITE;
/*!40000 ALTER TABLE `maestro_tipo_pago` DISABLE KEYS */;
INSERT INTO `maestro_tipo_pago` VALUES (1,'TICKET CONTADO'),(2,'TICKET CREDITO'),(3,'CHEQUE');
/*!40000 ALTER TABLE `maestro_tipo_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maestro_usuario`
--

DROP TABLE IF EXISTS `maestro_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maestro_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_rol` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `nombre_completo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `idtienda` int(11) DEFAULT NULL,
  `id_area` int(11) DEFAULT '0',
  PRIMARY KEY (`id_usuario`),
  KEY `fk_maestrousuario_gurol` (`id_rol`),
  KEY `fk_maestrousuario_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_maestrousuario_gurol` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`),
  CONSTRAINT `fk_maestrousuario_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maestro_usuario`
--

LOCK TABLES `maestro_usuario` WRITE;
/*!40000 ALTER TABLE `maestro_usuario` DISABLE KEYS */;
INSERT INTO `maestro_usuario` VALUES (1,'admin','admin',1,'2015-06-25','Administrador',1,0),(2,'planchado','123',1,'2015-09-03','planchado',1,4),(3,'ENTREGAR','123',1,'2015-09-03','ENTREGAR',1,0),(4,'Sastreria','123',1,'2015-09-03','Sastreria',1,1),(5,'Lavado','123',1,'2015-09-03','Lavado',1,2),(6,'Secado','123',1,'2015-09-03','Secado',1,3),(7,'Por Entregar','123',1,'2015-09-03','Por Entregar',1,5),(8,'planchado2','123',1,'2015-09-03','planchado2',2,4),(9,'ENTREGAR2','123',1,'2015-09-03','ENTREGAR2',2,0),(10,'Sastreria2','123',1,'2015-09-03','Sastreria2',2,1),(11,'Lavado2','123',1,'2015-09-03','Lavado2',2,2),(12,'Secado2','123',1,'2015-09-03','Secado2',2,3),(13,'Por Entregar2','123',1,'2015-09-03','Por Entregar2',2,5),(14,'planchado3','123',1,'2015-09-03','Por Entregar3',3,4),(15,'ENTREGAR3','123',1,'2015-09-03','ENTREGAR3',3,0),(16,'Sastreria3','123',1,'2015-09-03','Sastreria3',3,1),(17,'Lavado3','123',1,'2015-09-03','Lavado3',3,2),(18,'Secado3','123',1,'2015-09-03','Secado3',3,3),(19,'Por Entregar3','123',1,'2015-09-03','Por Entregar3',3,5);
/*!40000 ALTER TABLE `maestro_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_detalle`
--

DROP TABLE IF EXISTS `movimiento_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_detalle` (
  `idmovimiento_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `idmovimiento` int(11) NOT NULL DEFAULT '0',
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `cantidad` double(12,2) NOT NULL DEFAULT '0.00',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `iva` double(12,2) NOT NULL DEFAULT '0.00',
  `total` double(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idmovimiento_detalle`),
  KEY `fk_movimientodetalle_movimientoencabezado` (`idmovimiento`),
  KEY `fk_movimientodetalle_maestroinsumo` (`idinsumo`),
  CONSTRAINT `fk_movimientodetalle_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_movimientodetalle_movimientoencabezado` FOREIGN KEY (`idmovimiento`) REFERENCES `movimiento_encabezado` (`idmovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_detalle`
--

LOCK TABLES `movimiento_detalle` WRITE;
/*!40000 ALTER TABLE `movimiento_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimiento_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_encabezado`
--

DROP TABLE IF EXISTS `movimiento_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_encabezado` (
  `idmovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `idtipomovimiento` int(11) NOT NULL DEFAULT '0',
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `idproveedor` int(11) NOT NULL DEFAULT '0',
  `valor_neto` double(12,2) NOT NULL DEFAULT '0.00',
  `iva` double(12,2) NOT NULL DEFAULT '0.00',
  `valor_total` double NOT NULL DEFAULT '0',
  `documento` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmovimiento`),
  KEY `fk_movimientoencabezado_maestroproveedor` (`idproveedor`),
  KEY `fk_movimientoencabezado_maestrotipomovimiento` (`idtipomovimiento`),
  CONSTRAINT `fk_movimientoencabezado_maestroproveedor` FOREIGN KEY (`idproveedor`) REFERENCES `maestro_proveedor` (`idproveedor`),
  CONSTRAINT `fk_movimientoencabezado_maestrotipomovimiento` FOREIGN KEY (`idtipomovimiento`) REFERENCES `maestro_tipo_movimiento` (`idtipomovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_encabezado`
--

LOCK TABLES `movimiento_encabezado` WRITE;
/*!40000 ALTER TABLE `movimiento_encabezado` DISABLE KEYS */;
INSERT INTO `movimiento_encabezado` VALUES (1,'2019-09-10 12:00:41',1,1,2,0.00,0.00,590,111,1),(2,'2019-09-10 13:04:22',1,1,2,0.00,0.00,900,123,1),(3,'2019-09-10 13:05:56',1,1,2,0.00,0.00,940,123,1);
/*!40000 ALTER TABLE `movimiento_encabezado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento_temp`
--

DROP TABLE IF EXISTS `movimiento_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimiento_temp` (
  `idmovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idproveedor` int(11) NOT NULL,
  `idinsumo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` double(12,2) NOT NULL DEFAULT '0.00',
  `total` double(12,2) NOT NULL DEFAULT '0.00',
  `id_usuario` int(11) NOT NULL,
  `idtienda` int(11) NOT NULL,
  `descripcion_insumo` varchar(300) NOT NULL,
  `idtipomovimiento` int(11) NOT NULL,
  `documento` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmovimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento_temp`
--

LOCK TABLES `movimiento_temp` WRITE;
/*!40000 ALTER TABLE `movimiento_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimiento_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ot_encabezado`
--

DROP TABLE IF EXISTS `ot_encabezado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ot_encabezado` (
  `idorden` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `idfactura` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  `sastreria` int(11) NOT NULL DEFAULT '0',
  `lavado` int(11) NOT NULL DEFAULT '0',
  `secado` int(11) NOT NULL DEFAULT '0',
  `planchado` int(11) NOT NULL DEFAULT '0',
  `usuario_sastreria` int(11) NOT NULL DEFAULT '0',
  `usuario_lavado` int(11) NOT NULL DEFAULT '0',
  `usuario_secado` int(11) NOT NULL DEFAULT '0',
  `usuario_planchado` int(11) NOT NULL DEFAULT '0',
  `usuario_entregado` int(11) DEFAULT '0',
  `fecha_sastreria` datetime DEFAULT NULL,
  `fecha_lavado` datetime DEFAULT NULL,
  `fecha_secado` datetime DEFAULT NULL,
  `fecha_planchado` datetime DEFAULT NULL,
  `fecha_entregado` datetime DEFAULT NULL,
  `estado_sastreria` int(11) NOT NULL DEFAULT '0',
  `estado_lavado` int(11) NOT NULL DEFAULT '0',
  `estado_secado` int(11) NOT NULL DEFAULT '0',
  `estado_planchado` int(11) NOT NULL DEFAULT '0',
  `estado_entregado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idorden`),
  KEY `fk_factura_encabezado` (`idfactura`),
  CONSTRAINT `fk_factura_encabezado` FOREIGN KEY (`idfactura`) REFERENCES `factura_encabezado` (`idfactura`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ot_encabezado`
--

LOCK TABLES `ot_encabezado` WRITE;
/*!40000 ALTER TABLE `ot_encabezado` DISABLE KEYS */;
/*!40000 ALTER TABLE `ot_encabezado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saldos_cierre`
--

DROP TABLE IF EXISTS `saldos_cierre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saldos_cierre` (
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  `anio_mes` int(11) NOT NULL DEFAULT '0',
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  KEY `fk_saldoscierre_maestroinsumo` (`idinsumo`),
  KEY `fk_saldoscierre_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_saldoscierre_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_saldoscierre_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saldos_cierre`
--

LOCK TABLES `saldos_cierre` WRITE;
/*!40000 ALTER TABLE `saldos_cierre` DISABLE KEYS */;
/*!40000 ALTER TABLE `saldos_cierre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saldos_insumo`
--

DROP TABLE IF EXISTS `saldos_insumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saldos_insumo` (
  `idinsumo` int(11) NOT NULL DEFAULT '0',
  `idtienda` int(11) NOT NULL DEFAULT '0',
  `saldo` double(12,2) NOT NULL DEFAULT '0.00',
  KEY `fk_saldosinsumo_maestroinsumo` (`idinsumo`),
  KEY `fk_saldostienda_maestrotienda` (`idtienda`),
  CONSTRAINT `fk_saldosinsumo_maestroinsumo` FOREIGN KEY (`idinsumo`) REFERENCES `maestro_insumo` (`idinsumo`),
  CONSTRAINT `fk_saldostienda_maestrotienda` FOREIGN KEY (`idtienda`) REFERENCES `maestro_tienda` (`idtienda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saldos_insumo`
--

LOCK TABLES `saldos_insumo` WRITE;
/*!40000 ALTER TABLE `saldos_insumo` DISABLE KEYS */;
/*!40000 ALTER TABLE `saldos_insumo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-11 12:22:02
